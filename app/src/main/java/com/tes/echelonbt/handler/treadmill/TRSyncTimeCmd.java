package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TRSyncTimeCmd extends Command {
  Date datetime;
  
  Date validDate;
  
  public TRSyncTimeCmd(Date paramDate1, Date paramDate2) {
    this.datetime = paramDate1;
    this.validDate = paramDate2;
  }
  
  public ADData compactRequestData() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    calendar.setTime(this.datetime);
    this.commandData.appendByte((byte)(calendar.get(1) - 2000));
    this.commandData.appendByte((byte)(calendar.get(2) + 1));
    this.commandData.appendByte((byte)calendar.get(5));
    this.commandData.appendByte((byte)calendar.get(11));
    this.commandData.appendByte((byte)calendar.get(12));
    this.commandData.appendByte((byte)calendar.get(13));
    calendar.setTime(this.validDate);
    this.commandData.appendByte((byte)(calendar.get(1) - 2000));
    this.commandData.appendByte((byte)(calendar.get(2) + 1));
    this.commandData.appendByte((byte)calendar.get(5));
    this.commandData.appendByte((byte)1);
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -110;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRSyncTimeCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */