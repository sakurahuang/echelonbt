package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.TreadmillPeripheralListener;

public class TRGetFanSpeedLimitCmd extends Command {
  protected byte getActionCode() {
    return -89;
  }
  
  public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener trListener) {
    byte[] arrayOfByte = paramADData.bytes();
    ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[] { ADConverter.byteArrayToHexString(arrayOfByte) });
    final int max_fan_speed_level = toInteger(arrayOfByte[3]);
    if (trListener != null && trListener instanceof TreadmillPeripheralListener)
      Command.post(new Runnable() {
            public void run() {
              ((TreadmillPeripheralListener) trListener).onGetFanSpeedLimitResponse(max_fan_speed_level);
            }
          }); 
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRGetFanSpeedLimitCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */