package com.tes.echelonbt.handler;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class GetFTMPValidDateCmd extends Command {
  protected byte getActionCode() {
    return -90;
  }
  
  public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
    byte[] arrayOfByte = paramADData.bytes();
    ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[] { ADConverter.byteArrayToHexString(arrayOfByte) });
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    calendar.set(11, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    calendar.set(1, arrayOfByte[3] & 0x8CF);
    calendar.set(2, arrayOfByte[4] & 0xFE);
    calendar.set(5, arrayOfByte[5] & 0xFF);
    final Date date = calendar.getTime();
    if (listener != null)
      Command.post(new Runnable() {
            public void run() {
              listener.onGetFTMPValidDateResponse(date);
            }
          }); 
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/GetFTMPValidDateCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */