package com.tes.echelonbt.handler;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.santoble.Workout;
import com.tes.echelonbt.santoble.BLEPeripheral;

public class GetResistanceLevelCmd extends Command {
    protected byte getActionCode() {
        return -91;
    }

    public void handleReceivedData(ADData paramADData,BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int level = toInteger(arrayOfByte[3]);
        Workout.sharedInstance().setResistanceLevel(level);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetResistanceLevelResponse(level);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetResistanceLevelCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */