package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.TreadmillPeripheral;
import com.tes.echelonbt.santoble.TreadmillPeripheralListener;
import com.tes.echelonbt.santoble.Workout;

public class TRWorkoutStatusNotify extends Command {
    protected byte getActionCode() {
        return -47;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener trListener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int timestamp = toInteger(arrayOfByte[3], arrayOfByte[4]);
        final int distance = toInteger(arrayOfByte[5], arrayOfByte[6], arrayOfByte[7], arrayOfByte[8]);
        int i = distance;
        if (!Workout.sharedInstance().isProtocolUnitMetric())
            i = (int) (i * 1.609344D);
        final int calories = toInteger(arrayOfByte[9], arrayOfByte[10]);
        final int hr = toInteger(arrayOfByte[11]);
        final double distance_in_km = i / 1000.0D;
        Workout.sharedInstance().setTimestamp(timestamp);
        TreadmillPeripheral treadmillPeripheral = paramBLEPeripheral.getAsTreadmillPeripheral();
        if (treadmillPeripheral != null)
            treadmillPeripheral.replyNotify(this, arrayOfByte);
        if (trListener != null && trListener instanceof TreadmillPeripheralListener)
            Command.post(new Runnable() {
                public void run() {
                    ((TreadmillPeripheralListener)trListener).workoutStatusChanged(timestamp, distance_in_km, calories, hr);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRWorkoutStatusNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */