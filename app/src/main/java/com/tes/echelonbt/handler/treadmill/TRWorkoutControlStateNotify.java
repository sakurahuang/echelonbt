package com.tes.echelonbt.handler.treadmill;


import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;

import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.TreadmillPeripheral;

public class TRWorkoutControlStateNotify extends Command {
    protected byte getActionCode() {
        return -48;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int state = toInteger(arrayOfByte[3]);
        TreadmillPeripheral treadmillPeripheral = paramBLEPeripheral.getAsTreadmillPeripheral();
        if (treadmillPeripheral != null)
            treadmillPeripheral.replyNotify(this, arrayOfByte);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.controlStateChanged(state);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRWorkoutControlStateNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */