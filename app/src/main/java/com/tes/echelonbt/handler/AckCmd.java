package com.tes.echelonbt.handler;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;

public class AckCmd extends Command {
    static int COUNT;

    public ADData compactRequestData() {
        COUNT++;
        this.commandData.appendByte(toByte(COUNT));
        return super.compactRequestData();
    }

    protected byte getActionCode() {
        return -96;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onAckResponse();
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/AckCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */