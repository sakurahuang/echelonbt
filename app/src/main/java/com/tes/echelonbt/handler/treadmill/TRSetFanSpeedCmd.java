package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;

public class TRSetFanSpeedCmd extends Command {
  int mFanSpeedLevel = 0;
  
  public TRSetFanSpeedCmd(int paramInt) {
    this.mFanSpeedLevel = paramInt;
  }
  
  public ADData compactRequestData() {
    this.commandData.appendByte(toByte(this.mFanSpeedLevel));
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -77;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRSetFanSpeedCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */