package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.Workout;

public class TRSetSpeedCmd extends Command {
  double mSpeed = 0.0D;
  
  public TRSetSpeedCmd(double paramDouble) {
    this.mSpeed = paramDouble;
  }
  
  public ADData compactRequestData() {
    int j = (int)(this.mSpeed * 1000.0D) + 1;
    int i = j;
    if (!Workout.sharedInstance().isProtocolUnitMetric())
      i = (int)(j / 1.609344D); 
    this.commandData.appendByte(toByte(i >> 8));
    this.commandData.appendByte(toByte(i));
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -78;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRSetSpeedCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */