package com.tes.echelonbt.handler.Rower;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.Workout;
import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;

public class ROWorkoutStatusNotify extends Command {
    protected byte getActionCode() {
        return -47;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        byte b11 = arrayOfByte[3];
        byte timestamp = arrayOfByte[4];
        byte direction = arrayOfByte[5];
        final int count = arrayOfByte[6] << 24 & 0xFF000000 | arrayOfByte[7] << 16 & 0xFF0000 | arrayOfByte[8] << 8 & 0xFF00 | arrayOfByte[9] << 0 & 0xFF;
        final int spm = arrayOfByte[10] << 8 & 0xFF00 | arrayOfByte[11] << 0 & 0xFF;
        byte hr = arrayOfByte[12];

//        byte time500 = arrayOfByte[13];
//        byte calories = arrayOfByte[14];

        byte b6 = arrayOfByte[15];
        byte b1 = arrayOfByte[16];
        byte b8 = arrayOfByte[17];

//        byte b2 = arrayOfByte[18];
//        byte b5 = arrayOfByte[19];
        double speed = Workout.getSpeedMultiplier();
        final double distance_in_km = (b6 << 16 & 0xFF0000 | b1 << 8 & 0xFF00 | b8 << 0 & 0xFF) / 1000.0D;
        final double watt = Workout.sharedInstance().getWatt(b11);
        double calories = Workout.sharedInstance().getCalories();
        int time500=Workout.sharedInstance().getTimestamp()/500;
        if (Workout.sharedInstance().getPreviousCount() > b11)
            Workout.sharedInstance().resetWorkout();
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.workoutStatusChanged(timestamp, direction, count, spm, hr, speed, distance_in_km, time500, calories, watt);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/Rower/ROWorkoutStatusNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */