package com.tes.echelonbt.handler;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.santoble.BLEPeripheral;

public class SetWorkoutControlStateCmd extends Command {
    int mState = 0;

    public SetWorkoutControlStateCmd(int paramInt) {
        this.mState = paramInt;
    }

    public ADData compactRequestData() {
        this.commandData=new ADData();
        this.commandData.appendByte((byte) this.mState);
        return super.compactRequestData();
    }

    protected byte getActionCode() {
        return -80;
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/SetWorkoutControlStateCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */