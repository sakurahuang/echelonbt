package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.Workout;

public class TRGetDeviceInfoCmd extends Command {
    protected byte getActionCode() {
        return -95;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int modelID = toInteger(arrayOfByte[3]);
        final String hwVer = String.format("%02d.%02d", new Object[]{Integer.valueOf(toInteger(arrayOfByte[4])), Integer.valueOf(toInteger(arrayOfByte[5]))});
        final String fwVer = String.format("%02d.%02d.%02d", new Object[]{Integer.valueOf(toInteger(arrayOfByte[6])), Integer.valueOf(toInteger(arrayOfByte[7])), Integer.valueOf(toInteger(arrayOfByte[8]))});
        int i = toInteger(arrayOfByte[9]);
        if (paramBLEPeripheral != null) {
            if (modelID >= 16 && modelID < 32) {
                paramBLEPeripheral.setAsRowerMode();
            } else if (modelID >= 32) {
                paramBLEPeripheral.setAsTreadmillMode();
            }
        }
        Workout.sharedInstance().setModelID(modelID);
        Workout.sharedInstance().setFwVer(toInteger(arrayOfByte[6]), toInteger(arrayOfByte[7]), toInteger(arrayOfByte[8]));
        Workout.sharedInstance().setProtocolUnit(i);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetDeviceInfoResponse(modelID, hwVer, fwVer);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRGetDeviceInfoCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */