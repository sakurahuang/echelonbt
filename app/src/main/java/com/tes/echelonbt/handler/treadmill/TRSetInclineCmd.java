package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.handler.Command;

public class TRSetInclineCmd extends Command {
  int mIncline = 0;
  
  public TRSetInclineCmd(int paramInt) {
    this.mIncline = paramInt;
  }
  
  public ADData compactRequestData() {
    this.commandData.appendByte(toByte(this.mIncline));
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -79;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRSetInclineCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */