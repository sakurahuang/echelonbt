package com.tes.echelonbt.handler.treadmill;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.appdevice.adble.utility.ADData;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.santoble.BLEPeripheralListener;
import com.tes.echelonbt.santoble.Workout;
import com.tes.echelonbt.santoble.TreadmillPeripheralListener;
public class TRGetInclineCmd extends Command {
  protected byte getActionCode() {
    return -91;
  }
  
  public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener trListener) {
    byte[] arrayOfByte = paramADData.bytes();
    ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[] { ADConverter.byteArrayToHexString(arrayOfByte) });
    final int level = toInteger(arrayOfByte[3]);
    Workout.sharedInstance().setResistanceLevel(level);
    if (trListener != null && trListener instanceof TreadmillPeripheralListener)
      Command.post(new Runnable() {
            public void run() {
              ((TreadmillPeripheralListener) trListener).onGetInclineResponse(level);
            }
          }); 
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRGetInclineCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */