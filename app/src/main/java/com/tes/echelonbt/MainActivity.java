package com.tes.echelonbt;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.tes.echelonbt.santoble.BLEManager;
import com.tes.echelonbt.santoble.BLEManagerListener;
import com.tes.echelonbt.santoble.BLEPeripheral;
import com.tes.echelonbt.util.AppUtil;

import io.nlopez.smartlocation.SmartLocation;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements BLEManagerListener {

    boolean bAutoConnect = false;

    private ListView listview;

    String mDeviceMAC = null;

    private LayoutInflater mInflater;

    BaseAdapter mListviewAdapter = new BaseAdapter() {
        public int getCount() {
            return BLEManager.getInstance().getDiscoveredPeripherals().size();
        }

        public Object getItem(int param1Int) {
            return BLEManager.getInstance().getDiscoveredPeripherals().get(param1Int);
        }

        public long getItemId(int param1Int) {
            return 0L;
        }

        public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
            DeviceItem deviceItem;
            if (param1View == null) {
                param1View = MainActivity.this.mInflater.inflate(R.layout.layout_device_list_item, null);
                deviceItem = new DeviceItem((TextView) param1View.findViewById(R.id.textview));
                param1View.setTag(deviceItem);
            } else {
                deviceItem = (DeviceItem) param1View.getTag();
            }
            BLEPeripheral bLEPeripheral = BLEManager.getInstance().getDiscoveredPeripherals().get(param1Int);
            deviceItem.textview.setText(bLEPeripheral.getName());
            return param1View;
        }

        class DeviceItem {
            TextView textview;

            public DeviceItem(TextView textView) {
                this.textview = textView;
            }
        }
    };

    private void cancelBLE() {
        BLEManager.getInstance().stopScanPeripherals();
        BLEManager.getInstance().unregisterListener(this);
    }

    private void refreshListView() {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.mListviewAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void bleDidConnectPeripheral(BLEPeripheral paramBLEPeripheral) {
        BLEManager.getInstance().stopScanPeripherals();
        getSharedPreferences(MainActivity.class.getName(), 0).edit().putString("AUTO_CONNECT", paramBLEPeripheral.getAddress()).commit();
        startActivity(new Intent(MainActivity.this, ControlPaneActivity.class));
    }

    @Override
    public void bleDidDisconnectPeripheral(BLEPeripheral paramBLEPeripheral) {
        BLEManager.getInstance().stopScanPeripherals();
        refreshListView();
    }

    @Override
    public void bleDidDiscoverPeripheral(BLEPeripheral paramBLEPeripheral) {
        if (this.bAutoConnect && this.mDeviceMAC != null && paramBLEPeripheral != null && paramBLEPeripheral.getAddress() != null && paramBLEPeripheral.getAddress().equals(this.mDeviceMAC)) {
            BLEManager.getInstance().connectPeripheral(paramBLEPeripheral);
        } else {
            refreshListView();
        }
    }

    @Override
    public void bleDidInitialized() {
        BLEManager.getInstance().scanPeripherals();
        refreshListView();
    }

    @NeedsPermission({"android.permission.ACCESS_COARSE_LOCATION"})
    public void gpsSettingsRequest() {
        if (!SmartLocation.with(this).location().state().locationServicesEnabled()) {
            runOnUiThread(new Runnable() {
                public void run() {
                    (new AlertDialog.Builder((Context) MainActivity.this)).setTitle("Enable Location Service").setMessage("Some smartphones need Location Service enabled to scan BLE correctly. Do you want to enable Location Service now?").setNegativeButton("No, thanks.", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface param2DialogInterface, int param2Int) {
                            MainActivityPermissionsDispatcher.initBLEWithPermissionCheck(MainActivity.this);
                        }
                    }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface param2DialogInterface, int param2Int) {
                            Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                            MainActivity.this.startActivity(intent);
                        }
                    }).show();
                }
            });
        } else {
            MainActivityPermissionsDispatcher.initBLEWithPermissionCheck(this);
        }
    }

    @NeedsPermission({"android.permission.BLUETOOTH"})
    protected void initBLE() {
        if (!((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter().isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            BLEManager.getInstance().registerListener(this);
            BLEManager.getInstance().initialize((Context) this);
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_main);
        if (!AppUtil.isMTB()) {
            finish();
        }
        this.listview = (ListView) findViewById(R.id.listview);
        setTitle("ECHELON BT");
        this.mInflater = getLayoutInflater();
        this.listview.setAdapter((ListAdapter) this.mListviewAdapter);
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
                BLEManager.getInstance().connectPeripheral((BLEPeripheral) MainActivity.this.mListviewAdapter.getItem(param1Int));
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.menu_scan, paramMenu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        int i = paramMenuItem.getItemId();
        if (i == R.id.action_scan) {
            BLEManager.getInstance().stopScanPeripherals();
            MainActivityPermissionsDispatcher.gpsSettingsRequestWithPermissionCheck(this);
            refreshListView();
            return true;
        }
        if (i == R.id.action_auto) {
            this.bAutoConnect = !bAutoConnect;
            if (this.bAutoConnect) {
                paramMenuItem.setTitle("Manual");
                this.mDeviceMAC = getSharedPreferences(MainActivity.class.getName(), 0).getString("AUTO_CONNECT", null);
            } else {
                paramMenuItem.setTitle("Auto");
                this.mDeviceMAC = null;
            }
            return true;
        }
        return super.onOptionsItemSelected(paramMenuItem);
    }

    protected void onPause() {
        if (BLEManager.getInstance().getInitialized())
            cancelBLE();
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu paramMenu) {
        return super.onPrepareOptionsMenu(paramMenu);
    }

    public void onRequestPermissionsResult(int paramInt, @NonNull String[] paramArrayOfString, @NonNull int[] paramArrayOfint) {
        super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfint);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, paramInt, paramArrayOfint);
    }

    protected void onResume() {
        super.onResume();
        refreshListView();
        MainActivityPermissionsDispatcher.gpsSettingsRequestWithPermissionCheck(this);
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santo/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */