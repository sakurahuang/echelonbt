package com.tes.echelonbt.santoble;

import com.tes.echelonbt.util.TreadmillProgramSet;

import java.util.List;

public class Workout {
    private static final int WattTableMaxRPM = 100;

    private static Workout __shared_instance__;


    private static double mWheelDiameter = 0.0019812000000000002D;

    private static double mRpmRatio = 1.0D;

    private static double mDistancePerCount = mWheelDiameter * Math.PI * mRpmRatio;

    private static double mSpeedMultiplier = mDistancePerCount * 60.0D;


    protected double[] CaloriesTable = new double[]{
            0.072D, 0.072D, 0.084D, 0.096D, 0.118D, 0.136D, 0.162D, 0.185D, 0.21D, 0.23D,
            0.253D, 0.278D, 0.305D, 0.33D, 0.36D, 0.391D, 0.433D, 0.475D, 0.517D, 0.559D,
            0.601D, 0.643D, 0.685D, 0.727D, 0.769D, 0.811D, 0.853D, 0.895D, 0.937D, 0.979D,
            1.021D, 1.063D, 1.105D};

    protected double[][] WattTable;

    protected double[][] WattTable2;

    protected double[][] WattTable3;

    protected double[][] WattTable4;

    protected double[][] WattTable6;

    protected double[][] WattTableRower5;

    private double mCalories;

    private int mCountdownSeconds;

    private int mFWMainVer;

    private int mFWModVer;

    private int mFWSecVer;

    private int mIncline;

    private int mMaxIncline;

    private int mMaxResistanceLevel;

    private double mMaxSpeed;

    private int mMinIncline;

    private int mMinResistanceLevel;

    private double mMinSpeed;

    private int mModelID;

    private double mPreviosCount;

    private int mProtocolUnit;

    private int mResistanceLevel;

    private double mSpeed;

    private int mTimestamp;

    private TreadmillProgramRunner mTreadmillProgramRunner;

    private List<TreadmillProgramSet> mTreadmillProgramSets;

    public Workout() {
        double[] arrayOfDouble1 = {
                0.0D, 9.1D, 15.5D, 28.7D, 31.0D, 42.6D, 55.7D, 70.7D, 85.7D, 100.5D,
                115.4D};
        double[] arrayOfDouble2 = {
                0.0D, 12.9D, 19.1D, 35.2D, 39.0D, 54.4D, 71.7D, 95.6D, 111.1D, 130.7D,
                150.0D};
        double[] arrayOfDouble3 = {
                0.0D, 14.1D, 20.2D, 38.5D, 42.6D, 62.2D, 78.5D, 104.7D, 121.5D, 138.0D,
                158.0D};
        double[] arrayOfDouble4 = {
                0.0D, 14.8D, 20.8D, 40.0D, 45.0D, 65.5D, 82.5D, 105.6D, 122.0D, 143.2D,
                164.2D};
        double[] arrayOfDouble5 = {
                0.0D, 16.2D, 22.5D, 44.0D, 46.5D, 72.3D, 90.1D, 117.1D, 128.5D, 158.6D,
                181.5D};
        double[] arrayOfDouble6 = {
                0.0D, 17.9D, 23.9D, 47.2D, 49.0D, 76.8D, 97.1D, 131.5D, 142.0D, 176.5D,
                201.0D};
        double[] arrayOfDouble7 = {
                0.0D, 26.6D, 36.5D, 75.3D, 77.8D, 133.1D, 174.3D, 278.3D, 239.0D, 276.4D,
                348.5D};
        double[] arrayOfDouble8 = {
                0.0D, 31.6D, 50.0D, 105.8D, 103.5D, 181.6D, 242.8D, 358.7D, 330.0D, 420.0D,
                465.0D};
        this.WattTable = new double[][]{
                {
                        0.0D, 7.1D, 13.5D, 25.0D, 28.5D, 39.5D, 51.5D, 65.5D, 78.5D, 92.0D,
                        105.6D}, {
                0.0D, 7.1D, 13.5D, 25.0D, 28.5D, 39.5D, 51.5D, 65.5D, 78.5D, 92.0D,
                105.6D}, {
                0.0D, 7.6D, 14.0D, 26.0D, 29.0D, 40.0D, 52.3D, 66.0D, 79.0D, 92.5D,
                106.3D}, {
                0.0D, 8.1D, 14.5D, 27.0D, 29.6D, 40.5D, 53.0D, 66.8D, 80.3D, 94.1D,
                107.7D}, {
                0.0D, 8.7D, 15.0D, 27.8D, 30.1D, 41.3D, 54.0D, 67.6D, 82.6D, 97.5D,
                111.4D}, arrayOfDouble1, {
                0.0D, 9.6D, 16.0D, 29.0D, 31.7D, 44.4D, 57.5D, 73.0D, 88.3D, 103.8D,
                119.3D}, {
                0.0D, 10.2D, 16.5D, 30.0D, 33.5D, 45.9D, 60.6D, 75.8D, 92.1D, 108.0D,
                124.0D}, {
                0.0D, 10.8D, 17.0D, 30.5D, 35.0D, 47.3D, 62.2D, 79.1D, 96.0D, 113.0D,
                129.6D}, {
                0.0D, 11.3D, 17.5D, 32.0D, 35.5D, 48.8D, 64.2D, 81.5D, 99.1D, 116.0D,
                134.0D},
                {
                        0.0D, 11.9D, 18.0D, 33.0D, 36.2D, 50.6D, 66.4D, 88.2D, 102.8D, 120.5D,
                        138.7D}, {
                0.0D, 12.3D, 18.5D, 34.0D, 38.5D, 52.3D, 68.8D, 91.1D, 107.3D, 125.0D,
                144.0D}, arrayOfDouble2, {
                0.0D, 13.4D, 19.6D, 37.0D, 40.6D, 59.5D, 75.1D, 99.7D, 114.8D, 136.0D,
                157.0D}, arrayOfDouble3, arrayOfDouble4, {
                0.0D, 15.4D, 21.8D, 41.5D, 45.9D, 68.9D, 86.5D, 111.2D, 124.0D, 150.8D,
                172.7D}, arrayOfDouble5, {
                0.0D, 17.1D, 23.5D, 45.5D, 47.3D, 74.5D, 92.5D, 124.1D, 135.2D, 165.5D,
                189.8D}, arrayOfDouble6,
                {
                        0.0D, 18.5D, 24.1D, 49.7D, 51.0D, 82.5D, 102.7D, 140.7D, 150.8D, 185.0D,
                        212.0D}, {
                0.0D, 19.1D, 24.8D, 53.8D, 54.4D, 84.6D, 109.7D, 151.0D, 159.0D, 198.0D,
                226.3D}, {
                0.0D, 20.1D, 26.0D, 56.2D, 57.0D, 87.8D, 117.8D, 165.0D, 169.9D, 205.5D,
                241.0D}, {
                0.0D, 21.4D, 27.5D, 57.0D, 58.5D, 92.9D, 125.4D, 188.0D, 181.5D, 212.3D,
                256.0D}, {
                0.0D, 22.3D, 29.5D, 60.8D, 62.5D, 101.6D, 135.9D, 199.0D, 193.8D, 225.4D,
                274.0D}, {
                0.0D, 23.5D, 31.5D, 64.8D, 66.2D, 109.6D, 147.5D, 223.2D, 209.0D, 241.5D,
                293.5D}, {
                0.0D, 24.7D, 33.5D, 70.0D, 71.5D, 119.7D, 159.9D, 245.9D, 224.4D, 256.5D,
                312.8D}, arrayOfDouble7, {
                0.0D, 28.1D, 40.0D, 84.8D, 83.9D, 144.2D, 192.5D, 306.9D, 267.5D, 308.5D,
                381.0D}, {
                0.0D, 29.7D, 44.5D, 92.1D, 93.5D, 163.8D, 219.7D, 334.1D, 295.4D, 365.7D,
                422.2D},
                arrayOfDouble8, {
                0.0D, 33.1D, 57.0D, 117.5D, 114.8D, 207.4D, 280.4D, 375.2D, 367.2D, 470.4D,
                528.7D}, {
                0.0D, 36.7D, 62.0D, 125.1D, 126.9D, 239.8D, 320.2D, 406.4D, 425.0D, 520.2D,
                600.0D}};
        arrayOfDouble1 = new double[]{
                0.0D, 1.0D, 2.2D, 12.0D, 20.0D, 27.0D, 33.0D, 43.0D, 50.0D, 57.0D,
                65.0D};
        arrayOfDouble2 = new double[]{
                0.0D, 1.0D, 2.2D, 12.0D, 20.0D, 27.0D, 33.0D, 43.0D, 50.0D, 57.0D,
                65.0D};
        arrayOfDouble3 = new double[]{
                0.0D, 1.3D, 3.0D, 13.2D, 21.6D, 28.7D, 34.2D, 44.8D, 52.5D, 62.0D,
                71.0D};
        arrayOfDouble4 = new double[]{
                0.0D, 1.5D, 3.7D, 15.2D, 22.3D, 29.5D, 36.3D, 46.6D, 56.0D, 69.0D,
                74.0D};
        arrayOfDouble5 = new double[]{
                0.0D, 1.6D, 4.7D, 16.0D, 23.3D, 31.0D, 37.5D, 49.0D, 61.4D, 73.5D,
                76.0D};
        arrayOfDouble6 = new double[]{
                0.0D, 1.8D, 5.2D, 16.5D, 24.3D, 32.9D, 39.0D, 51.9D, 64.0D, 77.0D,
                80.8D};
        arrayOfDouble7 = new double[]{
                0.0D, 2.0D, 6.2D, 17.2D, 26.0D, 35.2D, 44.5D, 56.0D, 71.0D, 81.0D,
                86.7D};
        arrayOfDouble8 = new double[]{
                0.0D, 2.1D, 6.8D, 18.0D, 27.3D, 35.9D, 46.0D, 58.3D, 73.0D, 85.0D,
                90.0D};
        double[] arrayOfDouble9 = {
                0.0D, 2.2D, 7.3D, 19.0D, 28.0D, 37.5D, 48.2D, 61.2D, 77.4D, 90.4D,
                95.9D};
        double[] arrayOfDouble10 = {
                0.0D, 2.4D, 7.9D, 19.7D, 29.0D, 39.5D, 50.5D, 64.2D, 78.0D, 93.1D,
                101.0D};
        double[] arrayOfDouble11 = {
                0.0D, 2.6D, 8.5D, 20.5D, 30.3D, 42.0D, 55.0D, 67.7D, 81.4D, 97.5D,
                107.5D};
        double[] arrayOfDouble12 = {
                0.0D, 2.7D, 9.1D, 21.2D, 32.0D, 43.7D, 57.0D, 70.6D, 86.0D, 105.0D,
                113.0D};
        double[] arrayOfDouble13 = {
                0.0D, 2.9D, 9.6D, 21.7D, 34.0D, 47.5D, 60.0D, 75.2D, 91.0D, 110.0D,
                121.0D};
        double[] arrayOfDouble14 = {
                0.0D, 3.0D, 10.0D, 22.5D, 35.1D, 49.0D, 62.4D, 79.0D, 94.7D, 116.0D,
                126.0D};
        double[] arrayOfDouble15 = {
                0.0D, 3.2D, 10.4D, 24.0D, 36.6D, 52.5D, 66.0D, 84.0D, 104.0D, 121.0D,
                134.0D};
        double[] arrayOfDouble16 = {
                0.0D, 3.5D, 10.9D, 25.1D, 38.5D, 53.5D, 69.0D, 89.3D, 108.0D, 126.0D,
                141.0D};
        double[] arrayOfDouble17 = {
                0.0D, 3.7D, 11.5D, 26.0D, 41.0D, 56.7D, 75.0D, 94.5D, 112.7D, 135.0D,
                151.0D};
        double[] arrayOfDouble18 = {
                0.0D, 4.0D, 12.1D, 27.5D, 43.6D, 60.0D, 82.0D, 101.0D, 122.0D, 143.0D,
                162.8D};
        double[] arrayOfDouble19 = {
                0.0D, 4.2D, 12.7D, 29.7D, 46.7D, 67.0D, 87.0D, 109.2D, 132.7D, 154.0D,
                172.3D};
        double[] arrayOfDouble20 = {
                0.0D, 4.5D, 13.7D, 32.0D, 50.0D, 71.8D, 95.6D, 113.8D, 142.1D, 165.0D,
                185.0D};
        double[] arrayOfDouble21 = {
                0.0D, 4.7D, 14.9D, 34.5D, 54.2D, 77.0D, 101.0D, 127.0D, 150.8D, 180.0D,
                200.0D};
        double[] arrayOfDouble22 = {
                0.0D, 5.0D, 15.8D, 36.5D, 58.3D, 83.4D, 110.0D, 136.0D, 161.5D, 196.0D,
                213.5D};
        double[] arrayOfDouble23 = {
                0.0D, 5.6D, 17.0D, 39.5D, 64.3D, 88.8D, 123.0D, 154.0D, 182.0D, 210.0D,
                235.0D};
        double[] arrayOfDouble24 = {
                0.0D, 6.1D, 18.2D, 44.0D, 70.7D, 99.9D, 133.0D, 166.0D, 198.0D, 230.0D,
                252.0D};
        double[] arrayOfDouble25 = {
                0.0D, 6.8D, 19.4D, 49.0D, 79.0D, 108.8D, 147.0D, 185.0D, 217.0D, 255.0D,
                278.0D};
        double[] arrayOfDouble26 = {
                0.0D, 7.6D, 22.0D, 54.8D, 88.0D, 127.0D, 167.0D, 212.0D, 244.0D, 287.0D,
                305.0D};
        double[] arrayOfDouble27 = {
                0.0D, 8.7D, 26.0D, 62.0D, 100.0D, 145.0D, 190.0D, 242.0D, 281.0D, 315.0D,
                350.0D};
        double[] arrayOfDouble28 = {
                0.0D, 9.8D, 36.0D, 82.5D, 134.5D, 193.3D, 252.0D, 313.0D, 360.0D, 420.0D,
                460.0D};
        double[] arrayOfDouble29 = {
                0.0D, 10.5D, 43.0D, 95.0D, 158.0D, 228.0D, 299.0D, 374.0D, 403.8D, 480.0D,
                530.0D};
        double[] arrayOfDouble30 = {
                0.0D, 12.5D, 48.0D, 99.3D, 162.2D, 232.9D, 310.0D, 400.0D, 435.5D, 520.5D,
                580.0D};
        double[] arrayOfDouble31 = {
                0.0D, 13.0D, 53.0D, 102.0D, 165.0D, 239.0D, 318.0D, 420.0D, 470.1D, 560.0D,
                620.0D};
        this.WattTable2 = new double[][]{
                arrayOfDouble1, arrayOfDouble2, arrayOfDouble3, arrayOfDouble4, arrayOfDouble5, arrayOfDouble6, {
                0.0D, 1.9D, 5.7D, 16.9D, 25.0D, 34.0D, 42.0D, 53.7D, 66.6D, 79.0D,
                84.1D}, arrayOfDouble7, arrayOfDouble8, arrayOfDouble9,
                arrayOfDouble10, arrayOfDouble11, arrayOfDouble12, arrayOfDouble13, arrayOfDouble14, arrayOfDouble15, arrayOfDouble16, arrayOfDouble17, arrayOfDouble18, arrayOfDouble19,
                arrayOfDouble20, arrayOfDouble21, arrayOfDouble22, arrayOfDouble23, arrayOfDouble24, arrayOfDouble25, arrayOfDouble26, arrayOfDouble27, {
                0.0D, 9.2D, 30.0D, 71.0D, 114.2D, 162.6D, 216.0D, 275.0D, 317.0D, 358.0D,
                390.0D}, arrayOfDouble28,
                arrayOfDouble29, arrayOfDouble30, arrayOfDouble31};
        arrayOfDouble1 = new double[]{
                0.0D, 1.0D, 2.2D, 4.8D, 9.5D, 13.6D, 16.7D, 22.6D, 26.3D, 29.2D,
                47.0D};
        arrayOfDouble2 = new double[]{
                0.0D, 1.0D, 2.2D, 4.8D, 9.5D, 13.6D, 16.7D, 22.6D, 26.3D, 29.2D,
                47.0D};
        arrayOfDouble3 = new double[]{
                0.0D, 1.3D, 3.0D, 5.4D, 10.4D, 14.5D, 18.5D, 24.6D, 27.6D, 33.5D,
                49.5D};
        arrayOfDouble4 = new double[]{
                0.0D, 1.5D, 3.7D, 6.7D, 11.7D, 15.9D, 19.6D, 26.1D, 30.8D, 35.2D,
                51.2D};
        arrayOfDouble5 = new double[]{
                0.0D, 1.6D, 4.7D, 7.5D, 13.7D, 17.6D, 20.3D, 28.5D, 36.9D, 42.6D,
                57.2D};
        arrayOfDouble6 = new double[]{
                0.0D, 1.8D, 5.2D, 8.0D, 14.8D, 19.1D, 22.7D, 30.6D, 37.5D, 50.8D,
                61.8D};
        arrayOfDouble7 = new double[]{
                0.0D, 1.9D, 5.7D, 8.7D, 15.6D, 20.2D, 24.8D, 35.2D, 39.6D, 52.1D,
                65.3D};
        arrayOfDouble8 = new double[]{
                0.0D, 2.0D, 6.2D, 9.5D, 16.8D, 21.8D, 27.1D, 39.5D, 42.8D, 57.8D,
                68.4D};
        arrayOfDouble9 = new double[]{
                0.0D, 2.1D, 6.8D, 10.8D, 18.2D, 23.6D, 28.6D, 42.3D, 47.6D, 60.5D,
                72.1D};
        arrayOfDouble10 = new double[]{
                0.0D, 2.2D, 7.3D, 11.5D, 19.3D, 26.3D, 31.2D, 45.3D, 51.8D, 66.7D,
                76.8D};
        arrayOfDouble11 = new double[]{
                0.0D, 2.4D, 7.9D, 12.7D, 20.8D, 29.8D, 37.6D, 52.2D, 56.2D, 73.5D,
                83.6D};
        arrayOfDouble12 = new double[]{
                0.0D, 2.6D, 8.5D, 13.5D, 23.5D, 33.6D, 41.9D, 55.1D, 59.0D, 78.6D,
                89.7D};
        arrayOfDouble13 = new double[]{
                0.0D, 2.7D, 9.1D, 14.2D, 25.6D, 35.4D, 45.3D, 57.3D, 62.8D, 81.3D,
                95.0D};
        arrayOfDouble14 = new double[]{
                0.0D, 2.9D, 9.6D, 16.8D, 29.1D, 37.5D, 49.6D, 62.5D, 67.5D, 84.7D,
                99.3D};
        arrayOfDouble15 = new double[]{
                0.0D, 3.0D, 10.0D, 22.3D, 31.2D, 40.3D, 51.8D, 79.0D, 72.3D, 92.6D,
                108.2D};
        arrayOfDouble16 = new double[]{
                0.0D, 3.2D, 10.4D, 24.0D, 36.6D, 42.5D, 56.3D, 80.8D, 83.2D, 98.2D,
                123.5D};
        arrayOfDouble17 = new double[]{
                0.0D, 3.5D, 10.9D, 25.1D, 38.5D, 47.6D, 65.4D, 87.1D, 89.7D, 114.8D,
                136.8D};
        arrayOfDouble18 = new double[]{
                0.0D, 3.7D, 11.5D, 26.0D, 41.0D, 53.2D, 71.6D, 93.2D, 95.4D, 121.7D,
                149.2D};
        arrayOfDouble19 = new double[]{
                0.0D, 4.0D, 12.1D, 27.5D, 43.6D, 54.8D, 82.3D, 101.0D, 113.6D, 143.0D,
                162.8D};
        arrayOfDouble20 = new double[]{
                0.0D, 4.2D, 12.7D, 29.7D, 46.7D, 64.2D, 87.9D, 109.2D, 128.9D, 154.0D,
                172.3D};
        arrayOfDouble21 = new double[]{
                0.0D, 4.5D, 13.7D, 32.0D, 50.0D, 71.8D, 95.6D, 113.8D, 135.6D, 165.0D,
                185.0D};
        arrayOfDouble22 = new double[]{
                0.0D, 4.7D, 14.9D, 34.5D, 54.2D, 77.0D, 100.7D, 127.0D, 147.6D, 180.0D,
                200.0D};
        arrayOfDouble23 = new double[]{
                0.0D, 5.0D, 15.8D, 36.5D, 58.3D, 83.4D, 110.1D, 136.0D, 168.1D, 196.0D,
                213.5D};
        arrayOfDouble24 = new double[]{
                0.0D, 5.6D, 17.0D, 39.5D, 64.3D, 88.8D, 123.4D, 154.0D, 182.0D, 210.0D,
                235.0D};
        arrayOfDouble25 = new double[]{
                0.0D, 6.1D, 18.2D, 44.0D, 70.7D, 99.9D, 133.3D, 166.0D, 198.0D, 230.0D,
                253.5D};
        arrayOfDouble26 = new double[]{
                0.0D, 6.8D, 19.4D, 49.0D, 79.0D, 108.8D, 147.2D, 185.0D, 217.0D, 255.2D,
                278.0D};
        arrayOfDouble27 = new double[]{
                0.0D, 8.7D, 26.0D, 62.0D, 100.0D, 145.0D, 190.0D, 242.0D, 281.0D, 315.1D,
                350.0D};
        arrayOfDouble28 = new double[]{
                0.0D, 9.2D, 30.0D, 71.0D, 114.4D, 161.6D, 215.1D, 275.1D, 317.0D, 358.5D,
                390.0D};
        arrayOfDouble29 = new double[]{
                0.0D, 9.8D, 36.0D, 82.5D, 134.5D, 195.3D, 252.5D, 313.7D, 360.0D, 420.3D,
                460.0D};
        arrayOfDouble30 = new double[]{
                0.0D, 10.5D, 43.0D, 95.0D, 157.1D, 228.4D, 300.1D, 374.1D, 403.8D, 487.8D,
                540.0D};
        arrayOfDouble31 = new double[]{
                0.0D, 12.5D, 48.0D, 99.3D, 162.2D, 232.9D, 310.4D, 400.3D, 435.5D, 530.5D,
                589.0D};
        double[] arrayOfDouble32 = {
                0.0D, 13.0D, 53.0D, 102.0D, 170.3D, 242.0D, 320.0D, 427.9D, 475.2D, 570.0D,
                625.0D};
        this.WattTable3 = new double[][]{
                arrayOfDouble1, arrayOfDouble2, arrayOfDouble3, arrayOfDouble4, arrayOfDouble5, arrayOfDouble6, arrayOfDouble7, arrayOfDouble8, arrayOfDouble9, arrayOfDouble10,
                arrayOfDouble11, arrayOfDouble12, arrayOfDouble13, arrayOfDouble14, arrayOfDouble15, arrayOfDouble16, arrayOfDouble17, arrayOfDouble18, arrayOfDouble19, arrayOfDouble20,
                arrayOfDouble21, arrayOfDouble22, arrayOfDouble23, arrayOfDouble24, arrayOfDouble25, arrayOfDouble26, {
                0.0D, 7.6D, 22.0D, 54.8D, 88.0D, 127.0D, 167.0D, 212.0D, 244.0D, 287.0D,
                305.0D}, arrayOfDouble27, arrayOfDouble28, arrayOfDouble29,
                arrayOfDouble30, arrayOfDouble31, arrayOfDouble32};
        arrayOfDouble1 = new double[]{
                0.0D, 4.2D, 12.7D, 29.7D, 46.7D, 64.2D, 87.9D, 109.2D, 128.9D, 154.0D,
                172.3D};
        arrayOfDouble2 = new double[]{
                0.0D, 5.6D, 17.0D, 39.5D, 64.3D, 88.8D, 123.4D, 154.0D, 182.0D, 210.0D,
                235.0D};
        arrayOfDouble3 = new double[]{
                0.0D, 6.1D, 18.2D, 44.0D, 70.7D, 99.9D, 133.3D, 166.0D, 198.0D, 230.0D,
                253.5D};
        arrayOfDouble4 = new double[]{
                0.0D, 6.8D, 19.4D, 49.0D, 79.0D, 108.8D, 147.2D, 185.0D, 217.0D, 255.2D,
                278.0D};
        arrayOfDouble5 = new double[]{
                0.0D, 8.7D, 26.0D, 62.0D, 100.0D, 145.0D, 190.0D, 242.0D, 281.0D, 315.1D,
                350.0D};
        this.WattTable4 = new double[][]{
                {
                        0.0D, 1.0D, 2.2D, 4.8D, 9.5D, 13.6D, 16.7D, 22.6D, 26.3D, 29.2D,
                        47.0D}, {
                0.0D, 1.0D, 2.2D, 4.8D, 9.5D, 13.6D, 16.7D, 22.6D, 26.3D, 29.2D,
                47.0D}, {
                0.0D, 1.3D, 3.0D, 5.4D, 10.4D, 14.5D, 18.5D, 24.6D, 27.6D, 33.5D,
                49.5D}, {
                0.0D, 1.5D, 3.7D, 6.7D, 11.7D, 15.9D, 19.6D, 26.1D, 30.8D, 35.2D,
                51.2D}, {
                0.0D, 1.6D, 4.7D, 7.5D, 13.7D, 17.6D, 20.3D, 28.5D, 36.9D, 42.6D,
                57.2D}, {
                0.0D, 1.8D, 5.2D, 8.0D, 14.8D, 19.1D, 22.7D, 30.6D, 37.5D, 50.8D,
                61.8D}, {
                0.0D, 1.9D, 5.7D, 8.7D, 15.6D, 20.2D, 24.8D, 35.2D, 39.6D, 52.1D,
                65.3D}, {
                0.0D, 2.0D, 6.2D, 9.5D, 16.8D, 21.8D, 27.1D, 39.5D, 42.8D, 57.8D,
                68.4D}, {
                0.0D, 2.1D, 6.8D, 10.8D, 18.2D, 23.6D, 28.6D, 42.3D, 47.6D, 60.5D,
                72.1D}, {
                0.0D, 2.2D, 7.3D, 11.5D, 19.3D, 26.3D, 31.2D, 45.3D, 51.8D, 66.7D,
                76.8D},
                {
                        0.0D, 2.4D, 7.9D, 12.7D, 20.8D, 29.8D, 37.6D, 52.2D, 56.2D, 73.5D,
                        83.6D}, {
                0.0D, 2.6D, 8.5D, 13.5D, 23.5D, 33.6D, 41.9D, 55.1D, 59.0D, 78.6D,
                89.7D}, {
                0.0D, 2.7D, 9.1D, 14.2D, 25.6D, 35.4D, 45.3D, 57.3D, 62.8D, 81.3D,
                95.0D}, {
                0.0D, 2.9D, 9.6D, 16.8D, 29.1D, 37.5D, 49.6D, 62.5D, 67.5D, 84.7D,
                99.3D}, {
                0.0D, 3.0D, 10.0D, 22.3D, 31.2D, 40.3D, 51.8D, 79.0D, 72.3D, 92.6D,
                108.2D}, {
                0.0D, 3.2D, 10.4D, 24.0D, 36.6D, 42.5D, 56.3D, 80.8D, 83.2D, 98.2D,
                123.5D}, {
                0.0D, 3.5D, 10.9D, 25.1D, 38.5D, 47.6D, 65.4D, 87.1D, 89.7D, 114.8D,
                136.8D}, {
                0.0D, 3.7D, 11.5D, 26.0D, 41.0D, 53.2D, 71.6D, 93.2D, 95.4D, 121.7D,
                149.2D}, {
                0.0D, 4.0D, 12.1D, 27.5D, 43.6D, 54.8D, 82.3D, 101.0D, 113.6D, 143.0D,
                162.8D}, arrayOfDouble1,
                {
                        0.0D, 4.5D, 13.7D, 32.0D, 50.0D, 71.8D, 95.6D, 113.8D, 135.6D, 165.0D,
                        185.0D}, {
                0.0D, 4.7D, 14.9D, 34.5D, 54.2D, 77.0D, 100.7D, 127.0D, 147.6D, 180.0D,
                200.0D}, {
                0.0D, 5.0D, 15.8D, 36.5D, 58.3D, 83.4D, 110.1D, 136.0D, 168.1D, 196.0D,
                213.5D}, arrayOfDouble2, arrayOfDouble3, arrayOfDouble4, {
                0.0D, 7.6D, 22.0D, 54.8D, 88.0D, 127.0D, 167.0D, 212.0D, 244.0D, 287.0D,
                305.0D}, arrayOfDouble5, {
                0.0D, 9.2D, 30.0D, 71.0D, 114.4D, 161.6D, 215.1D, 275.1D, 317.0D, 358.5D,
                391.3D}, {
                0.0D, 9.8D, 36.0D, 82.5D, 134.5D, 195.3D, 252.5D, 313.7D, 360.0D, 420.3D,
                473.5D},
                {
                        0.0D, 10.5D, 43.0D, 95.0D, 157.1D, 228.4D, 300.1D, 374.1D, 403.8D, 460.3D,
                        522.5D}, {
                0.0D, 12.5D, 48.0D, 99.3D, 162.2D, 232.9D, 310.4D, 400.3D, 435.5D, 501.2D,
                573.0D}, {
                0.0D, 13.0D, 53.0D, 102.0D, 170.3D, 242.0D, 320.0D, 427.9D, 450.0D, 520.0D,
                600.0D}};
        arrayOfDouble1 = new double[]{
                0.0D, 0.0D, 30.0D, 62.0D, 88.0D, 108.0D, 119.0D, 128.0D, 141.0D, 153.0D,
                162.0D, 174.0D};
        arrayOfDouble2 = new double[]{
                0.0D, 0.0D, 33.5D, 69.0D, 105.0D, 136.0D, 151.0D, 160.0D, 174.0D, 179.0D,
                180.0D, 193.0D};
        arrayOfDouble3 = new double[]{
                0.0D, 0.0D, 38.0D, 77.0D, 124.0D, 165.0D, 189.0D, 217.0D, 224.0D, 232.0D,
                240.0D, 254.0D};
        arrayOfDouble4 = new double[]{
                0.0D, 0.0D, 38.5D, 78.0D, 128.0D, 168.0D, 201.0D, 221.0D, 231.0D, 243.0D,
                249.0D, 262.0D};
        arrayOfDouble5 = new double[]{
                0.0D, 0.0D, 56.0D, 102.0D, 177.0D, 228.0D, 289.0D, 294.0D, 301.0D, 317.0D,
                330.0D, 351.0D};
        this.WattTableRower5 = new double[][]{
                arrayOfDouble1, {
                0.0D, 0.0D, 30.0D, 62.0D, 88.0D, 108.0D, 119.0D, 128.0D, 141.0D, 153.0D,
                162.0D, 174.0D}, {
                0.0D, 0.0D, 31.0D, 64.0D, 90.0D, 113.0D, 123.0D, 133.0D, 144.0D, 155.0D,
                164.0D, 176.0D}, {
                0.0D, 0.0D, 31.5D, 65.0D, 93.0D, 117.0D, 129.0D, 137.0D, 148.0D, 158.0D,
                167.0D, 180.0D}, {
                0.0D, 0.0D, 32.0D, 66.0D, 96.0D, 122.0D, 134.0D, 143.0D, 153.0D, 161.0D,
                169.0D, 182.0D}, {
                0.0D, 0.0D, 32.5D, 67.0D, 98.0D, 127.0D, 138.0D, 151.0D, 161.0D, 168.0D,
                170.0D, 184.0D}, {
                0.0D, 0.0D, 33.0D, 68.0D, 103.0D, 133.0D, 145.0D, 157.0D, 167.0D, 172.0D,
                174.0D, 188.0D}, arrayOfDouble2, {
                0.0D, 0.0D, 34.0D, 70.0D, 107.0D, 140.0D, 159.0D, 164.0D, 177.0D, 184.0D,
                186.0D, 198.0D}, {
                0.0D, 0.0D, 34.5D, 71.0D, 110.0D, 144.0D, 164.0D, 168.0D, 182.0D, 188.0D,
                190.0D, 205.0D},
                {
                        0.0D, 0.0D, 35.0D, 72.0D, 113.0D, 148.0D, 169.0D, 178.0D, 194.0D, 202.0D,
                        209.0D, 220.0D}, {
                0.0D, 0.0D, 35.5D, 73.5D, 117.0D, 155.0D, 174.0D, 185.0D, 199.0D, 209.0D,
                217.0D, 227.0D}, {
                0.0D, 0.0D, 36.0D, 75.0D, 120.0D, 158.0D, 179.0D, 188.0D, 205.0D, 217.0D,
                225.0D, 234.0D}, {
                0.0D, 0.0D, 37.0D, 76.5D, 122.0D, 162.0D, 186.0D, 202.0D, 216.0D, 225.0D,
                231.0D, 243.0D}, arrayOfDouble3, arrayOfDouble4, {
                0.0D, 0.0D, 39.5D, 79.0D, 132.0D, 175.0D, 208.0D, 228.0D, 239.0D, 251.0D,
                261.0D, 272.0D}, {
                0.0D, 0.0D, 40.5D, 80.5D, 138.0D, 180.0D, 219.0D, 234.0D, 246.0D, 259.0D,
                271.0D, 281.0D}, {
                0.0D, 0.0D, 41.0D, 82.0D, 145.0D, 187.0D, 229.0D, 246.0D, 252.0D, 264.0D,
                278.0D, 291.0D}, {
                0.0D, 0.0D, 41.5D, 85.0D, 148.0D, 202.0D, 239.0D, 253.0D, 261.0D, 270.0D,
                284.0D, 299.0D},
                {
                        0.0D, 0.0D, 42.0D, 87.0D, 153.0D, 205.0D, 249.0D, 259.0D, 266.0D, 278.0D,
                        291.0D, 310.0D}, {
                0.0D, 0.0D, 47.0D, 92.0D, 160.0D, 208.0D, 255.0D, 266.0D, 272.0D, 283.0D,
                296.0D, 315.0D}, {
                0.0D, 0.0D, 48.0D, 95.0D, 168.0D, 215.0D, 269.0D, 276.0D, 282.0D, 298.0D,
                307.0D, 325.0D}, {
                0.0D, 0.0D, 51.0D, 100.0D, 173.0D, 218.0D, 279.0D, 283.0D, 290.0D, 305.0D,
                320.0D, 341.0D}, arrayOfDouble5, {
                0.0D, 0.0D, 59.0D, 108.0D, 184.0D, 236.0D, 298.0D, 303.0D, 312.0D, 323.0D,
                337.0D, 367.0D}, {
                0.0D, 0.0D, 61.0D, 115.0D, 188.0D, 238.0D, 309.0D, 317.0D, 325.0D, 339.0D,
                353.0D, 380.0D}, {
                0.0D, 0.0D, 63.0D, 125.0D, 193.0D, 248.0D, 319.0D, 329.0D, 338.0D, 347.0D,
                364.0D, 389.0D}, {
                0.0D, 0.0D, 70.0D, 132.0D, 196.0D, 256.0D, 324.0D, 340.0D, 351.0D, 375.0D,
                389.0D, 408.0D}, {
                0.0D, 0.0D, 75.0D, 136.0D, 208.0D, 258.0D, 329.0D, 350.0D, 360.0D, 382.0D,
                402.0D, 421.0D},
                {
                        0.0D, 0.0D, 82.0D, 142.0D, 213.0D, 262.0D, 335.0D, 357.0D, 367.0D, 396.0D,
                        414.0D, 431.0D}, {
                0.0D, 0.0D, 86.0D, 152.0D, 216.0D, 266.0D, 339.0D, 362.0D, 372.0D, 415.0D,
                430.0D, 445.0D}, {
                0.0D, 0.0D, 90.0D, 158.0D, 223.0D, 268.0D, 344.0D, 368.0D, 399.0D, 430.0D,
                444.0D, 460.0D}};
        arrayOfDouble1 = new double[]{
                0.0D, 0.5D, 1.0D, 2.0D, 3.0D, 4.0D, 5.0D, 6.0D, 7.0D, 10.0D,
                12.0D};
        arrayOfDouble2 = new double[]{
                0.0D, 1.0D, 1.5D, 3.0D, 4.0D, 8.0D, 9.0D, 10.0D, 12.0D, 20.0D,
                18.0D};
        arrayOfDouble3 = new double[]{
                0.0D, 2.0D, 3.0D, 8.0D, 9.0D, 16.0D, 21.0D, 26.0D, 29.0D, 30.0D,
                48.0D};
        arrayOfDouble4 = new double[]{
                0.0D, 2.5D, 4.0D, 10.0D, 13.0D, 23.0D, 30.0D, 37.0D, 43.0D, 45.0D,
                60.0D};
        arrayOfDouble5 = new double[]{
                0.0D, 3.5D, 8.0D, 17.0D, 28.0D, 40.0D, 55.0D, 68.0D, 79.0D, 82.0D,
                110.0D};
        arrayOfDouble6 = new double[]{
                0.0D, 5.0D, 16.0D, 32.0D, 50.0D, 72.0D, 98.0D, 125.0D, 140.0D, 170.0D,
                215.0D};
        arrayOfDouble7 = new double[]{
                0.0D, 5.5D, 19.0D, 37.0D, 58.0D, 85.0D, 111.0D, 140.0D, 170.0D, 200.0D,
                240.0D};
        arrayOfDouble8 = new double[]{
                0.0D, 6.0D, 20.0D, 43.0D, 65.0D, 95.0D, 130.0D, 160.0D, 190.0D, 230.0D,
                280.0D};
        arrayOfDouble9 = new double[]{
                0.0D, 6.5D, 22.0D, 47.0D, 75.0D, 107.0D, 145.0D, 178.0D, 215.0D, 250.0D,
                310.0D};
        arrayOfDouble10 = new double[]{
                0.0D, 8.0D, 29.0D, 58.0D, 95.0D, 139.0D, 186.0D, 239.0D, 285.0D, 335.0D,
                409.0D};
        arrayOfDouble11 = new double[]{
                0.0D, 9.0D, 33.0D, 69.0D, 110.0D, 163.0D, 215.0D, 275.0D, 332.0D, 400.0D,
                470.0D};
        arrayOfDouble12 = new double[]{
                0.0D, 9.5D, 36.0D, 72.0D, 119.0D, 172.0D, 232.0D, 292.0D, 350.0D, 420.0D,
                508.0D};
        arrayOfDouble13 = new double[]{
                0.0D, 10.5D, 39.0D, 80.0D, 132.0D, 190.0D, 260.0D, 328.0D, 400.0D, 470.0D,
                563.0D};
        arrayOfDouble14 = new double[]{
                0.0D, 11.0D, 41.0D, 84.0D, 138.0D, 201.0D, 270.0D, 345.0D, 420.0D, 505.0D,
                590.0D};
        arrayOfDouble15 = new double[]{
                0.0D, 11.5D, 42.0D, 87.0D, 143.0D, 210.0D, 283.0D, 360.0D, 445.0D, 530.0D,
                620.0D};
        arrayOfDouble16 = new double[]{
                0.0D, 14.5D, 51.0D, 108.0D, 178.0D, 262.0D, 353.0D, 451.0D, 552.0D, 670.0D,
                785.0D};
        arrayOfDouble17 = new double[]{
                0.0D, 15.0D, 53.0D, 110.0D, 185.0D, 270.0D, 360.0D, 462.0D, 573.0D, 690.0D,
                810.0D};
        arrayOfDouble18 = new double[]{
                0.0D, 16.0D, 56.0D, 118.0D, 190.0D, 280.0D, 380.0D, 490.0D, 602.0D, 725.0D,
                850.0D};
        this.WattTable6 = new double[][]{
                {
                        0.0D, 0.5D, 1.0D, 2.0D, 3.0D, 4.0D, 5.0D, 6.0D, 7.0D, 10.0D,
                        12.0D}, arrayOfDouble1, arrayOfDouble2, {
                0.0D, 1.5D, 2.0D, 5.0D, 6.0D, 10.0D, 15.0D, 16.0D, 20.0D, 25.0D,
                30.0D}, arrayOfDouble3, arrayOfDouble4, {
                0.0D, 3.0D, 6.0D, 13.0D, 20.0D, 32.0D, 41.0D, 52.0D, 60.0D, 65.0D,
                80.0D}, arrayOfDouble5, {
                0.0D, 4.0D, 10.0D, 22.0D, 35.0D, 50.0D, 70.0D, 86.0D, 102.0D, 113.0D,
                140.0D}, {
                0.0D, 4.5D, 13.0D, 28.0D, 41.0D, 60.0D, 83.0D, 105.0D, 125.0D, 130.0D,
                170.0D},
                arrayOfDouble6, arrayOfDouble7, arrayOfDouble8, arrayOfDouble9, {
                0.0D, 7.0D, 24.0D, 50.0D, 82.0D, 118.0D, 159.0D, 202.0D, 240.0D, 280.0D,
                340.0D}, {
                0.0D, 7.5D, 27.0D, 55.0D, 89.0D, 128.0D, 173.0D, 217.0D, 260.0D, 305.0D,
                370.0D}, arrayOfDouble10, {
                0.0D, 8.5D, 31.0D, 65.0D, 103.0D, 150.0D, 203.0D, 255.0D, 305.0D, 360.0D,
                440.0D}, arrayOfDouble11, arrayOfDouble12,
                {
                        0.0D, 10.0D, 37.0D, 78.0D, 122.0D, 182.0D, 245.0D, 313.0D, 378.0D, 450.0D,
                        530.0D}, arrayOfDouble13, arrayOfDouble14, arrayOfDouble15, {
                0.0D, 12.0D, 44.0D, 92.0D, 150.0D, 218.0D, 295.0D, 378.0D, 463.0D, 555.0D,
                650.0D}, {
                0.0D, 12.5D, 46.0D, 95.0D, 155.0D, 228.0D, 305.0D, 395.0D, 485.0D, 575.0D,
                678.0D}, {
                0.0D, 13.0D, 47.0D, 97.0D, 162.0D, 238.0D, 318.0D, 408.0D, 502.0D, 600.0D,
                705.0D}, {
                0.0D, 13.5D, 48.5D, 101.0D, 168.0D, 245.0D, 330.0D, 420.0D, 515.0D, 620.0D,
                720.0D}, {
                0.0D, 14.0D, 49.7D, 105.0D, 172.0D, 253.0D, 340.0D, 440.0D, 535.0D, 645.0D,
                758.0D}, arrayOfDouble16,
                arrayOfDouble17, {
                0.0D, 15.5D, 54.0D, 115.0D, 187.0D, 275.0D, 373.0D, 478.0D, 585.0D, 710.0D,
                830.0D}, arrayOfDouble18};
        this.mCalories = 0.0D;
        this.mPreviosCount = 0.0D;
        this.mMaxResistanceLevel = 32;
        this.mMinResistanceLevel = 1;
        this.mMaxIncline = 20;
        this.mMinIncline = 0;
        this.mMaxSpeed = 20.0D;
        this.mMinSpeed = 1.0D;
        this.mResistanceLevel = this.mMinResistanceLevel;
        this.mIncline = this.mMinIncline;
        this.mSpeed = this.mMinSpeed;
        this.mCountdownSeconds = 3;
        this.mModelID = 0;
        this.mFWMainVer = 0;
        this.mFWSecVer = 0;
        this.mFWModVer = 0;
        this.mProtocolUnit = 0;
        this.mTimestamp = 0;
        this.mTreadmillProgramSets = null;
        this.mTreadmillProgramRunner = null;
    }

    private static void calcDistanceAndDistanceValues() {
        mDistancePerCount = mWheelDiameter * Math.PI * mRpmRatio;
        mSpeedMultiplier = mDistancePerCount * 60.0D;
    }

    public static double getDistancePerCount() {
        return mDistancePerCount;
    }

    public static double getRpmRatio() {
        return mRpmRatio;
    }

    public static double getSpeedMultiplier() {
        return mSpeedMultiplier;
    }

    public static double getWheelDiameter() {
        return mWheelDiameter;
    }

    private double[][] pickWattTable() {
        int i = this.mFWModVer;
        if (i == 6)
            return this.WattTable6;
        if (i == 4)
            return this.WattTable4;
        if (i == 3)
            return this.WattTable3;
        i = this.mFWMainVer;
        return (i >= 8) ? this.WattTable3 : ((i > 5) ? this.WattTable2 : this.WattTable);
    }

    public static void setRpmRatio(double paramDouble) {
        mRpmRatio = paramDouble;
        calcDistanceAndDistanceValues();
    }

    public static void setWheelDiameterInInch(double paramDouble) {
        setWheelDiameterInKM(paramDouble * 2.54D / 100000.0D);
    }

    public static void setWheelDiameterInKM(double paramDouble) {
        mWheelDiameter = paramDouble;
        calcDistanceAndDistanceValues();
    }

    public static Workout sharedInstance() {
        if (__shared_instance__ == null) {
            __shared_instance__ = new Workout();
        }
        return __shared_instance__;
    }

    public void addCaloriesWithCurrentCount(int paramInt) {
        int j = this.mResistanceLevel;
        int i = j;
        if (j < 0)
            i = 0;
        double[] arrayOfDouble = this.CaloriesTable;
        j = i;
        if (i >= arrayOfDouble.length)
            j = arrayOfDouble.length - 1;
        double d1 = this.CaloriesTable[j];
        double d3 = paramInt;
        double d2 = this.mPreviosCount;
        this.mPreviosCount = d3;
        this.mCalories += (d3 - d2) * d1;
    }

    public int availableIncline(int paramInt) {
        int j = this.mMinIncline;
        int i = paramInt;
        if (paramInt < j)
            i = j;
        j = this.mMaxIncline;
        paramInt = i;
        if (i > j)
            paramInt = j;
        return paramInt;
    }

    public int availableResistanceLevel(int paramInt) {
        int j = this.mMinResistanceLevel;
        int i = paramInt;
        if (paramInt < j)
            i = j;
        j = this.mMaxResistanceLevel;
        paramInt = i;
        if (i > j)
            paramInt = j;
        return paramInt;
    }

    public double availableSpeed(double paramDouble) {
        double d2 = this.mMinSpeed;
        double d1 = paramDouble;
        if (paramDouble < d2)
            d1 = d2;
        d2 = this.mMaxSpeed;
        paramDouble = d1;
        if (d1 > d2)
            paramDouble = d2;
        return paramDouble;
    }

    public double getCalories() {
        return this.mCalories;
    }

    public int getModelID() {
        return this.mModelID;
    }

    public double getPreviousCount() {
        return this.mPreviosCount;
    }

    public int getProtocolUnit() {
        return this.mProtocolUnit;
    }

    public double getRowerWatt(int paramInt) {
        double[][] arrayOfDouble1 = this.WattTableRower5;
        int j = this.mResistanceLevel;
        int i = j;
        if (j < 0)
            i = 0;
        j = i;
        if (i >= arrayOfDouble1.length)
            j = arrayOfDouble1.length - 1;
        double[] arrayOfDouble = arrayOfDouble1[j];
        i = paramInt / 5;
        if (i >= 11) {
            double d1 = arrayOfDouble[11];
            return paramInt / 55 * d1;
        }
        double d = arrayOfDouble[i];
        return d + (arrayOfDouble[i + 1] - d) / 5 * (paramInt % 5);
    }

    public int getTimestamp() {
        return this.mTimestamp;
    }

    public List<TreadmillProgramSet> getTreadmillProgramSets() {
        return this.mTreadmillProgramSets;
    }

    public double getWatt(int paramInt) {
        if (isRower())
            return getRowerWatt(paramInt);
        double[][] arrayOfDouble1 = pickWattTable();
        int j = this.mResistanceLevel;
        int i = j;
        if (j < 0)
            i = 0;
        j = i;
        if (i >= arrayOfDouble1.length)
            j = arrayOfDouble1.length - 1;
        double[] arrayOfDouble = arrayOfDouble1[j];
        i = paramInt / 10;
        if (i >= 10) {
            double d1 = arrayOfDouble[arrayOfDouble.length - 1];
            return paramInt / 100.0D * d1;
        }
        double d = arrayOfDouble[i];
        return d + (arrayOfDouble[i + 1] - d) / 10.0D * (paramInt % 10);
    }

    public boolean isProtocolUnitMetric() {
        boolean bool;
        if (this.mProtocolUnit == 0) {
            bool = true;
        } else {
            bool = false;
        }
        return bool;
    }

    public boolean isRower() {
        int i = this.mModelID;
        return (i >= 16 && i < 32);
    }

    public boolean isTreadmill() {
        int i = this.mModelID;
        return (i >= 32 && i < 48);
    }

    public void resetWorkout() {
        this.mCalories = 0.0D;
        this.mPreviosCount = 0.0D;
        this.mTimestamp = 0;
        this.mTreadmillProgramSets = null;
    }

    public void setCountdownSeconds(int paramInt) {
        this.mCountdownSeconds = paramInt;
    }

    public void setFwVer(int paramInt1, int paramInt2, int paramInt3) {
        this.mFWMainVer = paramInt1;
        this.mFWSecVer = paramInt2;
        this.mFWModVer = paramInt3;
    }

    public void setIncline(int paramInt) {
        this.mIncline = paramInt;
    }

    public void setInclineRange(int paramInt1, int paramInt2) {
        this.mMaxIncline = paramInt1;
        this.mMinIncline = paramInt2;
    }

    public void setModelID(int paramInt) {
        this.mModelID = paramInt;
    }

    public void setProtocolUnit(int paramInt) {
        this.mProtocolUnit = paramInt;
    }

    public void setResistanceLevel(int paramInt) {
        this.mResistanceLevel = paramInt;
    }

    public void setResistanceLevelRange(int paramInt1, int paramInt2) {
        this.mMaxResistanceLevel = paramInt1;
        this.mMinResistanceLevel = paramInt2;
    }

    public void setSpeed(double paramDouble) {
        this.mSpeed = paramDouble;
    }

    public void setSpeedRange(double paramDouble1, double paramDouble2) {
        this.mMaxSpeed = paramDouble1;
        this.mMinSpeed = paramDouble2;
    }

    public void setTimestamp(int paramInt) {
        if (mTreadmillProgramSets == null || mTreadmillProgramSets.size() <= 0)
            return;
        __shared_instance__.mTreadmillProgramRunner = new TreadmillProgramRunner();
        __shared_instance__.mTreadmillProgramRunner.run();
    }

    public void setTreadmillProgramSets(List<TreadmillProgramSet> paramList) {
        this.mTreadmillProgramSets = paramList;
    }

    public class ControlState {
        public static final int Countdown = 17;

        public static final int Pause = 2;

        public static final int Reset = 3;

        public static final int Start = 1;

        public static final int Stop = 0;
    }

    public class ModelIDs {
        public static final int Bike = 0;

        public static final int Rower = 16;

        public static final int Treadmill = 32;
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/Workout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */