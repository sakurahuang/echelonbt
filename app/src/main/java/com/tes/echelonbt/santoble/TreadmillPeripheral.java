package com.tes.echelonbt.santoble;

import androidx.annotation.NonNull;

import com.tes.echelonbt.handler.Command;
import com.tes.echelonbt.handler.GetWorkoutControlStateCmd;
import com.tes.echelonbt.handler.SetWorkoutControlStateCmd;
import com.tes.echelonbt.handler.treadmill.TRGetFanSpeedLevelCmd;
import com.tes.echelonbt.handler.treadmill.TRGetFanSpeedLimitCmd;
import com.tes.echelonbt.handler.treadmill.TRGetInclineCmd;
import com.tes.echelonbt.handler.treadmill.TRGetLimitsCmd;
import com.tes.echelonbt.handler.treadmill.TRGetSpeedCmd;
import com.tes.echelonbt.handler.treadmill.TRInclineChangedNotify;
import com.tes.echelonbt.handler.treadmill.TRSetFanSpeedCmd;
import com.tes.echelonbt.handler.treadmill.TRSpeedChangedNotify;
import com.tes.echelonbt.handler.treadmill.TRUserKeyPressedNotify;
import com.tes.echelonbt.handler.treadmill.TRWorkoutControlStateNotify;
import com.tes.echelonbt.handler.treadmill.TRWorkoutStatusNotify;
import com.tes.echelonbt.util.TreadmillProgramSet;
import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.appdevice.adble.utility.ADConverter;

import java.util.Date;
import java.util.List;

public class TreadmillPeripheral {
    BLEPeripheral mBLEPeripheral = null;

    TreadmillPeripheral(@NonNull BLEPeripheral paramBLEPeripheral) {
        this.mBLEPeripheral = paramBLEPeripheral;
    }

    public void ack() {
        this.mBLEPeripheral.ack();
    }

    public String getAddress() {
        return this.mBLEPeripheral.getAddress();
    }

    public void getDeviceInfo() {
        this.mBLEPeripheral.getDeviceInfo();
    }

    public void getErrorLog() {
        this.mBLEPeripheral.getErrorLog();
    }

    public void getFTMPValidDate() {
        this.mBLEPeripheral.getFTMPValidDate();
    }

    public void getFanSpeedLevel() {
        TRGetFanSpeedLevelCmd tRGetFanSpeedLevelCmd = new TRGetFanSpeedLevelCmd();
        this.mBLEPeripheral.writeValue((Command) tRGetFanSpeedLevelCmd);
    }

    public void getFanSpeedLimit() {
        TRGetFanSpeedLimitCmd tRGetFanSpeedLimitCmd = new TRGetFanSpeedLimitCmd();
        this.mBLEPeripheral.writeValue((Command) tRGetFanSpeedLimitCmd);
    }

    public void getIncline() {
        TRGetInclineCmd tRGetInclineCmd = new TRGetInclineCmd();
        this.mBLEPeripheral.writeValue((Command) tRGetInclineCmd);
    }

    public void getLimits() {
        TRGetLimitsCmd tRGetLimitsCmd = new TRGetLimitsCmd();
        this.mBLEPeripheral.writeValue((Command) tRGetLimitsCmd);
    }

    public String getName() {
        return this.mBLEPeripheral.getName();
    }

    public int getPeripheralState() {
        return this.mBLEPeripheral.getPeripheralState();
    }

    public void getSleepSetting() {
        this.mBLEPeripheral.getSleepSetting();
    }

    public void getSpeed() {
        TRGetSpeedCmd tRGetSpeedCmd = new TRGetSpeedCmd();
        this.mBLEPeripheral.writeValue((Command) tRGetSpeedCmd);
    }

    public void getWorkoutControlState() {
        GetWorkoutControlStateCmd getWorkoutControlStateCmd = new GetWorkoutControlStateCmd();
        this.mBLEPeripheral.writeValue((Command) getWorkoutControlStateCmd);
    }

    public boolean isConnected() {
        return this.mBLEPeripheral.isConnected();
    }

    public boolean isInitialized() {
        return this.mBLEPeripheral.isInitialized();
    }

    public void pauseWorkout() {
        setWorkoutControlState(2);
    }

    public void replyNotify(Command paramCommand, byte[] paramArrayOfbyte) {
        if (paramCommand instanceof TRInclineChangedNotify || paramCommand instanceof TRSpeedChangedNotify || paramCommand instanceof TRWorkoutControlStateNotify || paramCommand instanceof TRWorkoutStatusNotify || paramCommand instanceof TRUserKeyPressedNotify) {
            ADLog.i(getClass().getName(), "replyNofity %s", new Object[]{ADConverter.byteArrayToHexString(paramArrayOfbyte)});
            this.mBLEPeripheral.writeValue(paramArrayOfbyte);
        }
    }

    public void setFanSpeedLevel(int paramInt) {
        TRSetFanSpeedCmd tRSetFanSpeedCmd = new TRSetFanSpeedCmd(paramInt);
        this.mBLEPeripheral.writeValue((Command) tRSetFanSpeedCmd);
    }

    public void setListener(BLEPeripheralListener paramBLEPeripheralListener) {
        this.mBLEPeripheral.setListener(paramBLEPeripheralListener);
    }

    public void setSleepSetting(int paramInt) {
        this.mBLEPeripheral.setSleepSetting(paramInt);
    }

    public void setWorkoutControlState(int paramInt) {
        SetWorkoutControlStateCmd setWorkoutControlStateCmd = new SetWorkoutControlStateCmd(paramInt);
        this.mBLEPeripheral.writeValue((Command) setWorkoutControlStateCmd);
    }

    public void startWorkout() {
        setWorkoutControlState(1);
    }

    public void startWorkout(@NonNull List<TreadmillProgramSet> paramList) {
        if (paramList == null)
            return;
        Workout.sharedInstance().setTreadmillProgramSets(paramList);
        startWorkout();
    }

    public void stopWorkout() {
        setWorkoutControlState(0);
        Workout.sharedInstance().resetWorkout();
    }

    public void syncTime(Date paramDate1, Date paramDate2) {
        this.mBLEPeripheral.syncTime(paramDate1, paramDate2);
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/TreadmillPeripheral.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */