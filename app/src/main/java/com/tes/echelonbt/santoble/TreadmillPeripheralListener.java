package com.tes.echelonbt.santoble;

public interface TreadmillPeripheralListener extends BLEPeripheralListener {
  void inclineChanged(int paramInt);
  
  void onGetFanSpeedLevelResponse(int paramInt);
  
  void onGetFanSpeedLimitResponse(int paramInt);
  
  void onGetInclineResponse(int paramInt);
  
  void onGetLimitsResponse(int paramInt1, int paramInt2, double paramDouble1, double paramDouble2, int paramInt3);
  
  void onGetSpeedResponse(double paramDouble);
  
  void speedChanged(double paramDouble);
  
  void userKeyPressed(int paramInt1, int paramInt2);
  
  void workoutStatusChanged(int paramInt1, double paramDouble, int paramInt2, int paramInt3);
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/TreadmillPeripheralListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */