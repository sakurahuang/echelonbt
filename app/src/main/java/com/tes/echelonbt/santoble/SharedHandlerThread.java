package com.tes.echelonbt.santoble;

import android.os.Handler;
import android.os.HandlerThread;

public class SharedHandlerThread {
    static HandlerThread handlerThread;

    public static Handler createHandler() {
        return new Handler(getSharedHandlerThread().getLooper());
    }

    public static HandlerThread getSharedHandlerThread() {
        if (handlerThread == null) {
            handlerThread = new HandlerThread("Echelon Lib Thread");
            handlerThread.start();
        }

        return handlerThread;
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/SharedHandlerThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */