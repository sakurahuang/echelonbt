package com.tes.echelonbt.santoble;

public class BLE {
  public class ConnectionState {
    public static final int Connected = 2;
    
    public static final int Connecting = 1;
    
    public static final int Disconnected = 0;
  }
  
  public class Protocol {
    public static final int ActionIndex = 1;
    
    public static final int DataIndex = 3;
    
    public static final int LengthIndex = 2;
    
    public static final int MinCmdLen = 4;
    
    public static final byte Starter = -16;
    
    public static final int StarterIndex = 0;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/BLE.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */