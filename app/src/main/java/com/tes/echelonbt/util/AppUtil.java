package com.tes.echelonbt.util;

import android.os.Build;

public class AppUtil {


    /**
     *
     *是否是瑞讯的板子
     * @return
     */
    public static boolean isMTB() {
        return "rk3288_mtb813".contains(Build.MODEL) || "rk3288_mtb818".contains(Build.MODEL);
    }
}
