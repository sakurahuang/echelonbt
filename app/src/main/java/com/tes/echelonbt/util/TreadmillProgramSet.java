package com.tes.echelonbt.util;

public class TreadmillProgramSet {
  int durationSeconds;
  
  int targetIncline;
  
  double targetSpeed;
  
  public TreadmillProgramSet(int paramInt1, int paramInt2, double paramDouble) {
    this.durationSeconds = paramInt1;
    this.targetIncline = paramInt2;
    this.targetSpeed = paramDouble;
  }
  
  public int getDurationSeconds() {
    return this.durationSeconds;
  }
  
  public int getTargetIncline() {
    return this.targetIncline;
  }
  
  public double getTargetSpeed() {
    return this.targetSpeed;
  }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/util/TreadmillProgramSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */