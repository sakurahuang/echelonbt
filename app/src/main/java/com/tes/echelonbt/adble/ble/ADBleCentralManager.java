package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;

import com.tes.echelonbt.appdevice.adble.utility.ADLog;
import com.tes.echelonbt.santoble.SharedHandlerThread;

import androidx.annotation.NonNull;

import com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheral;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.tes.echelonbt.appdevice.adble.ble.ADBleCentralManagerCallback;
import com.tes.echelonbt.appdevice.adble.ble.ADBleService;
import com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheralConnectionStautsCallback;

@TargetApi(18)
public class ADBleCentralManager {
    private static String TAG = "ADBle";

    private static Context mApplicationContext;

    private static ADBleCentralManager mInstance;

    private boolean isScanning = false;

    private final ConcurrentHashMap<String, ADBlePeripheral> mBlePeripherals = new ConcurrentHashMap<String, ADBlePeripheral>();

    private ADBleService mBleService = null;

    private BluetoothAdapter mBluetoothAdapter = null;

    private ADBleCentralManagerCallback mCallback = null;

    private ADBlePeripheralConnectionStautsCallback mConnectionStautsCallback = new ADBlePeripheralConnectionStautsCallback() {
        public void didConnectionStatusChanged(ADBlePeripheral param1ADBlePeripheral, int param1Int) {
            if (mCallback != null)
                mCallback.bleCentralManagerDidConnectPeripheral(param1ADBlePeripheral);
        }
    };

    private Handler mHandler = SharedHandlerThread.createHandler();

    private boolean mInitializationState;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context param1Context, Intent param1Intent) {
            if (param1Intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                switch (param1Intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648)) {
                    default:
                        return;
                    case 13:
                        ADLog.v(ADBleCentralManager.TAG, "STATE_TURNING_OFF", new Object[0]);
                    case 12:
                        ADLog.v(ADBleCentralManager.TAG, "STATE_ON", new Object[0]);
                        if (ADBleCentralManager.this.isScanning)
                            ADBleCentralManager.this.scan();
                    case 11:
                        ADLog.v(ADBleCentralManager.TAG, "STATE_TURNING_ON", new Object[0]);
                    case 10:
                        break;
                }
                ADLog.v(ADBleCentralManager.TAG, "STATE_OFF", new Object[0]);
            }
        }
    };

    private ScanCallback mScanCallBack = new ScanCallback() {
        public void onBatchScanResults(List<ScanResult> param1List) {
            super.onBatchScanResults(param1List);
        }

        public void onScanFailed(int param1Int) {
            super.onScanFailed(param1Int);
        }

        public void onScanResult(int param1Int, ScanResult param1ScanResult) {
            if (param1ScanResult != null) {
                if (!mScanedAddresses.contains(param1ScanResult.getDevice().getAddress())) {
                    mScanedAddresses.add(param1ScanResult.getDevice().getAddress());
                }
                ScanRecord record = param1ScanResult.getScanRecord();
                mCallback.bleCentralManagerDidDiscover(getBlePeripheral(param1ScanResult.getDevice()), param1Int, record);
            }

        }
    };

    private final List<String> mScanedAddresses = new ArrayList<String>();

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
            ADBleService.LocalBinder binder = (ADBleService.LocalBinder) param1IBinder;
            mBleService = binder.getService();
            mCallback.bleCentralManagerDidInitialized();
        }

        public void onServiceDisconnected(ComponentName param1ComponentName) {
            ADBleCentralManager.getInstance().setCallBack(null);
        }
    };

    private Intent mServiceIntent = null;

    private ADBlePeripheral getBlePeripheral(@NonNull BluetoothDevice paramBluetoothDevice) {
        ADBlePeripheral aDBlePeripheral = null;
        if (this.mBlePeripherals.containsKey(paramBluetoothDevice.getAddress())) {
            aDBlePeripheral = this.mBlePeripherals.get(paramBluetoothDevice.getAddress());
        } else {
            ADBlePeripheral aDBlePeripheral1 = new ADBlePeripheral(paramBluetoothDevice, this.mBleService);
            this.mBlePeripherals.put(paramBluetoothDevice.getAddress(), aDBlePeripheral1);
            aDBlePeripheral = aDBlePeripheral1;
        }
        return aDBlePeripheral;
    }

    public static ADBleCentralManager getInstance() {
        if (mInstance == null)
            mInstance = new ADBleCentralManager();
        return mInstance;
    }

    private void requestBluetoothEnable() {
        BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent intent = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mApplicationContext.startActivity(intent);
        }
    }

    public void cancelPeripheralConnection(@NonNull ADBlePeripheral paramADBlePeripheral) {
        if (mApplicationContext != null && this.mBleService != null) {
            if (paramADBlePeripheral != null) {
                if (!this.mBluetoothAdapter.isEnabled()) {
                    requestBluetoothEnable();
                } else {
                    paramADBlePeripheral.cancelConnect();
                }
                return;
            }
            throw new IllegalArgumentException("peripheral = null");
        }
        throw new IllegalArgumentException("not initialized");
    }

    public boolean cancelScan() {
        if (mApplicationContext != null && this.mBleService != null) {
            BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
            if (bluetoothAdapter != null && this.isScanning) {
                if (!bluetoothAdapter.isEnabled()) {
                    requestBluetoothEnable();
                } else {
                    BluetoothLeScanner bluetoothLeScanner = this.mBluetoothAdapter.getBluetoothLeScanner();
                    if (bluetoothLeScanner != null)
                        bluetoothLeScanner.stopScan(this.mScanCallBack);
                }
                this.isScanning = false;
                return true;
            }
        }
        return false;
    }

    public void connectPeripheral(@NonNull ADBlePeripheral paramADBlePeripheral) {
        if (mApplicationContext != null && this.mBleService != null) {
            if (paramADBlePeripheral != null) {
                if (!this.mBluetoothAdapter.isEnabled()) {
                    requestBluetoothEnable();
                } else {
                    paramADBlePeripheral.setConnectionStautsCallback(this.mConnectionStautsCallback);
                    paramADBlePeripheral.connect();
                }
                return;
            }
            throw new IllegalArgumentException("peripheral = null");
        }
        throw new IllegalArgumentException("not initialized");
    }

    public void connectPeripheral(@NonNull String paramString) {
        BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
        if (bluetoothAdapter != null)
            connectPeripheral(getBlePeripheral(bluetoothAdapter.getRemoteDevice(paramString)));
    }

    public boolean getInitializationState() {
        return this.mInitializationState;
    }

    public void initialize(@NonNull Context paramContext) {
        if (paramContext != null) {
            mApplicationContext = paramContext.getApplicationContext();
            this.mBluetoothAdapter = ((BluetoothManager) mApplicationContext.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
            if (this.mServiceIntent != null) {
                try {
                    mApplicationContext.unbindService(this.mServiceConnection);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                try {
                    paramContext.unregisterReceiver(this.mReceiver);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            this.mServiceIntent = new Intent(mApplicationContext, ADBleService.class);
            mApplicationContext.bindService(this.mServiceIntent, this.mServiceConnection, 1);
            IntentFilter intentFilter = new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
            paramContext.registerReceiver(this.mReceiver, intentFilter);
            return;
        }
        throw new IllegalArgumentException("applicationContext = null");
    }

    public boolean isScanning() {
        return this.isScanning;
    }

    public boolean scan() {
        if (mApplicationContext != null && this.mBleService != null) {
            if (this.mBluetoothAdapter != null && !this.isScanning) {
                this.mScanedAddresses.clear();
                this.mBlePeripherals.clear();
                this.isScanning = true;
                if (!this.mBluetoothAdapter.isEnabled()) {
                    requestBluetoothEnable();
                } else {
                    BluetoothLeScanner bluetoothLeScanner = this.mBluetoothAdapter.getBluetoothLeScanner();
                    if (bluetoothLeScanner != null)
                        bluetoothLeScanner.startScan(this.mScanCallBack);
                }
                return this.isScanning;
            }
            return false;
        }
        throw new IllegalArgumentException("not initialized");
    }

    public void setCallBack(ADBleCentralManagerCallback paramADBleCentralManagerCallback) {
        mCallback = paramADBleCentralManagerCallback;
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBleCentralManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */