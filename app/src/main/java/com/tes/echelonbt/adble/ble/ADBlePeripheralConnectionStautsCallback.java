package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;

@TargetApi(18)
interface ADBlePeripheralConnectionStautsCallback {
  void didConnectionStatusChanged(ADBlePeripheral paramADBlePeripheral, int paramInt);
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBlePeripheralConnectionStautsCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */