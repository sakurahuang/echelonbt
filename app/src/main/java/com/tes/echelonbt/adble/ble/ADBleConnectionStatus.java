package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;

@TargetApi(18)
public class ADBleConnectionStatus {
  public static final int Connected = 2;
  
  public static final int Connecting = 1;
  
  public static final int Disconnected = 0;
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBleConnectionStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */