package com.tes.echelonbt.appdevice.adble.utility;

import java.io.Serializable;
import java.util.Arrays;

public class ADData implements Serializable, Cloneable {
    protected byte[] mBytes;

    public ADData() {
        this(new byte[0]);
    }

    public ADData(ADData paramADData) {
        this(paramADData.bytes());
    }

    public ADData(byte[] paramArrayOfbyte) {
        this.mBytes = paramArrayOfbyte;
    }

    public void appendByte(byte paramByte) {
        setLength(this.mBytes.length + 1);
        byte[] arrayOfByte = this.mBytes;
        arrayOfByte[arrayOfByte.length - 1] = (byte) paramByte;
    }

    public void appendBytes(byte[] paramArrayOfbyte) {
        int i = this.mBytes.length;
        setLength(paramArrayOfbyte.length + i);
        System.arraycopy(paramArrayOfbyte, 0, this.mBytes, i, paramArrayOfbyte.length);
    }

    public void appendData(ADData paramADData) {
        appendBytes(paramADData.bytes());
    }

    public byte[] bytes() {
        return this.mBytes;
    }

    public Object clone() {
        return this;
    }

    public boolean equals(byte[] paramObject) {

        return !!Arrays.equals(this.mBytes, paramObject);
    }

    public byte getByte(int paramInt) {
        if (paramInt < mBytes.length)
            return mBytes[paramInt];
        throw new ArrayIndexOutOfBoundsException(paramInt);
    }

    public int hashCode() {
        return 31 + Arrays.hashCode(this.mBytes);
    }

    public boolean isEqualToData(ADData paramADData) {
        return equals(paramADData);
    }

    public int length() {
        return this.mBytes.length;
    }

    public void setBytes(byte[] paramArrayOfbyte) {
        this.mBytes = paramArrayOfbyte;
    }

    public void setData(ADData paramADData) {
        setBytes(paramADData.bytes());
    }

    public void setLength(int paramInt) {
        byte[] arrayOfByte2 = new byte[paramInt];
        byte[] arrayOfByte1 = this.mBytes;
        int i = paramInt;
        if (paramInt > arrayOfByte1.length)
            i = arrayOfByte1.length;
        System.arraycopy(this.mBytes, 0, arrayOfByte2, 0, i);
        this.mBytes = arrayOfByte2;
    }

    public ADData subdata(int paramInt) {
        byte[] arrayOfByte = this.mBytes;
        if (paramInt <= arrayOfByte.length)
            return subdata(paramInt, arrayOfByte.length - paramInt);
        throw new ArrayIndexOutOfBoundsException(paramInt);
    }

    public ADData subdata(int paramInt1, int paramInt2) {
        byte[] arrayOfByte = this.mBytes;
        if (paramInt1 + paramInt2 <= arrayOfByte.length) {
            byte[] arrayOfByte1 = new byte[paramInt2];
            System.arraycopy(arrayOfByte, paramInt1, arrayOfByte1, 0, paramInt2);
            return new ADData(arrayOfByte1);
        }
        throw new ArrayIndexOutOfBoundsException(paramInt1);
    }

    public String toString() {
        return ADConverter.byteArrayToHexString(this.mBytes);
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/utility/ADData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */