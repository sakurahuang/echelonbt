package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;

import androidx.annotation.NonNull;

import com.tes.echelonbt.appdevice.adble.utility.ADLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

@TargetApi(18)
public class ADBleService extends Service {
    // 描述标识
    private static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    private static String TAG = "ADBle";

    private final IBinder mBinder = (IBinder) new LocalBinder();

    private final ConcurrentLinkedQueue<ADBleOperator> mBleOperators = new ConcurrentLinkedQueue<ADBleOperator>();

    private final ConcurrentHashMap<BluetoothDevice, BluetoothGatt> mBluetoothDeviceGattPairs = new ConcurrentHashMap<BluetoothDevice, BluetoothGatt>();

    private ADBleOperator mCurrentOperator = null;

    private Handler mHandler;

    private HandlerThread mThread;

    private void clearOperator(@NonNull BluetoothDevice paramBluetoothDevice) {
        ArrayList<ADBleOperator> arrayList = new ArrayList();
        for (ADBleOperator aDBleOperator1 : this.mBleOperators) {
            if (aDBleOperator1.getBluetoothDevice() == paramBluetoothDevice)
                arrayList.add(aDBleOperator1);
        }
        this.mBleOperators.removeAll(arrayList);
        ADBleOperator aDBleOperator = this.mCurrentOperator;
        if (aDBleOperator != null && aDBleOperator.getBluetoothDevice() == paramBluetoothDevice) {
            this.mCurrentOperator = null;
            runNext();
        }
    }

    public BluetoothGatt getBluetoothGatt(@NonNull BluetoothDevice paramBluetoothDevice) {
        BluetoothGatt bluetoothGatt = null;
        if (this.mBluetoothDeviceGattPairs.containsKey(paramBluetoothDevice)) {
            bluetoothGatt = this.mBluetoothDeviceGattPairs.get(paramBluetoothDevice);
        } else {
            paramBluetoothDevice = null;
        }
        return bluetoothGatt;
    }

    private void runNext() {
        if (mCurrentOperator == null) {
            if (!mBleOperators.isEmpty()) {
                ADBleOperator adBleOperator = mBleOperators.poll();
                this.mCurrentOperator = adBleOperator;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCurrentOperator.run();
                    }
                });
            }
        }
    }

    void cancelConnect(@NonNull BluetoothDevice paramBluetoothDevice) {
        clearOperator(paramBluetoothDevice);
        if (getBluetoothGatt(paramBluetoothDevice) != null) {
            getBluetoothGatt(paramBluetoothDevice).disconnect();
        }
    }

    void connect(@NonNull BluetoothDevice paramBluetoothDevice, BluetoothGattCallback paramBluetoothGattCallback) {
        clearOperator(paramBluetoothDevice);
        this.mBleOperators.offer(new ADBleConnectOperator(paramBluetoothDevice, paramBluetoothGattCallback));
        runNext();

    }

    void discoverServices(@NonNull BluetoothDevice paramBluetoothDevice) {
        this.mBleOperators.offer(new ADBleDiscoverServicesOperator(paramBluetoothDevice));
        runNext();
    }

    List<BluetoothGattService> getSupportedGattServices(@NonNull BluetoothDevice paramBluetoothDevice) {
        BluetoothGatt bluetoothGatt = getBluetoothGatt(paramBluetoothDevice);
        return (bluetoothGatt == null) ? null : bluetoothGatt.getServices();
    }

    public IBinder onBind(Intent paramIntent) {
        ADLog.v(TAG, "onBind", new Object[0]);
        return this.mBinder;
    }

    public void onCreate() {
        ADLog.v(TAG, "onCreate", new Object[0]);
        startForeground(0, null);
        this.mThread = new HandlerThread("adbleService.thread");
        this.mThread.start();
        this.mHandler = new Handler(this.mThread.getLooper());
    }

    public void onDestroy() {
        ADLog.v(TAG, "onDestroy()", new Object[0]);
        HandlerThread handlerThread = this.mThread;
        if (handlerThread != null)
            handlerThread.quit();
        super.onDestroy();
    }

    void onDisconnect(@NonNull BluetoothDevice paramBluetoothDevice) {
        clearOperator(paramBluetoothDevice);
        BluetoothGatt bluetoothGatt = getBluetoothGatt(paramBluetoothDevice);
        if (bluetoothGatt == null)
            return;
        bluetoothGatt.close();
        this.mBluetoothDeviceGattPairs.remove(paramBluetoothDevice);
    }

    void onOperatorFinish() {
        mCurrentOperator = null;
        runNext();
    }

    public boolean onUnbind(Intent paramIntent) {
        ADLog.v(TAG, "onUnbind", new Object[0]);
        return super.onUnbind(paramIntent);
    }

    void readCharacteristic(@NonNull BluetoothDevice paramBluetoothDevice, @NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic) {
        this.mBleOperators.offer(new ADBleReadCharacteristicOperator(paramBluetoothDevice, paramBluetoothGattCharacteristic));
        runNext();
    }

    void readRssi(@NonNull BluetoothDevice paramBluetoothDevice) {
        this.mBleOperators.offer(new ADBleReadRemoteRssiOperator(paramBluetoothDevice));
        runNext();
    }

    void setCharacteristicNotification(@NonNull BluetoothDevice paramBluetoothDevice, @NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic, boolean paramBoolean) {
        this.mBleOperators.offer(new ADBleSetCharacteristicNotificationOperator(paramBluetoothDevice, paramBluetoothGattCharacteristic, paramBoolean));
        runNext();
    }

    void writeCharacteristic(@NonNull BluetoothDevice paramBluetoothDevice, @NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic, @NonNull byte[] paramArrayOfbyte, com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheral.ADWriteCharacteristicCallback paramADWriteCharacteristicCallback) {
        this.mBleOperators.offer(new ADBleWriteCharacteristicOperator(paramBluetoothDevice, paramBluetoothGattCharacteristic, paramArrayOfbyte, paramADWriteCharacteristicCallback));
        runNext();
    }

    class ADBleConnectOperator extends ADBleOperator {
        private final BluetoothGattCallback mBluetoothGattCallback;

        ADBleConnectOperator(BluetoothDevice param1BluetoothDevice, BluetoothGattCallback param1BluetoothGattCallback) {
            super(param1BluetoothDevice);
            this.mBluetoothGattCallback = param1BluetoothGattCallback;
        }

        public void run() {
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt != null) {
                bluetoothGatt.connect();
            } else {
                bluetoothGatt = getBluetoothDevice().connectGatt((Context) ADBleService.this, false, this.mBluetoothGattCallback);
                if (bluetoothGatt != null)
                    ADBleService.this.mBluetoothDeviceGattPairs.put(getBluetoothDevice(), bluetoothGatt);
            }
        }
    }

    class ADBleDiscoverServicesOperator extends ADBleOperator {
        ADBleDiscoverServicesOperator(BluetoothDevice param1BluetoothDevice) {
            super(param1BluetoothDevice);
        }

        public void run() {
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt == null)
                return;
            bluetoothGatt.discoverServices();
        }
    }

    abstract class ADBleOperator implements Runnable {
        private final BluetoothDevice mBluetoothDevice;

        public ADBleOperator(BluetoothDevice param1BluetoothDevice) {
            this.mBluetoothDevice = param1BluetoothDevice;
        }

        public BluetoothDevice getBluetoothDevice() {
            return this.mBluetoothDevice;
        }

        public void run() {
        }
    }

    class ADBleReadCharacteristicOperator extends ADBleOperator {
        private final BluetoothGattCharacteristic mCharacteristic;

        ADBleReadCharacteristicOperator(@NonNull BluetoothDevice param1BluetoothDevice, BluetoothGattCharacteristic param1BluetoothGattCharacteristic) {
            super(param1BluetoothDevice);
            this.mCharacteristic = param1BluetoothGattCharacteristic;
        }

        public void run() {
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt == null)
                return;
            BluetoothGattCharacteristic bluetoothGattCharacteristic = this.mCharacteristic;
            if (bluetoothGattCharacteristic == null)
                return;
            bluetoothGatt.readCharacteristic(bluetoothGattCharacteristic);
        }
    }

    class ADBleReadRemoteRssiOperator extends ADBleOperator {
        ADBleReadRemoteRssiOperator(BluetoothDevice param1BluetoothDevice) {
            super(param1BluetoothDevice);
        }

        public void run() {
            ADLog.v(ADBleService.TAG, "%s readRemoteRssi", new Object[]{ADBleOperator.class.getName()});
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt == null)
                return;
            bluetoothGatt.readRemoteRssi();
        }
    }

    class ADBleSetCharacteristicNotificationOperator extends ADBleOperator {
        private final BluetoothGattCharacteristic mCharacteristic;

        private final boolean mEnabled;

        ADBleSetCharacteristicNotificationOperator(@NonNull BluetoothDevice param1BluetoothDevice, BluetoothGattCharacteristic param1BluetoothGattCharacteristic, boolean param1Boolean) {
            super(param1BluetoothDevice);
            this.mCharacteristic = param1BluetoothGattCharacteristic;
            this.mEnabled = param1Boolean;
        }

        public void run() {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt == null)
                return;
            BluetoothGattCharacteristic bluetoothGattCharacteristic = this.mCharacteristic;
            if (bluetoothGattCharacteristic == null)
                return;
            bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, this.mEnabled);
            BluetoothGattDescriptor bluetoothGattDescriptor = this.mCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            if (bluetoothGattDescriptor != null) {
                byte[] arrayOfByte = bluetoothGattDescriptor.getValue();
                if (this.mEnabled) {
                    if (!Arrays.equals(arrayOfByte, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE))
                        bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                } else if (!Arrays.equals(arrayOfByte, BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)) {
                    bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                }
                ADLog.v(ADBleService.TAG, "%s writeDescriptor", new Object[]{ADBleOperator.class.getName()});
                bluetoothGatt.writeDescriptor(bluetoothGattDescriptor);
            }
        }
    }

    class ADBleWriteCharacteristicOperator extends ADBleOperator {
        private final byte[] mBytes;

        private final ADBlePeripheral.ADWriteCharacteristicCallback mCallback;

        private final BluetoothGattCharacteristic mCharacteristic;

        ADBleWriteCharacteristicOperator(@NonNull BluetoothDevice param1BluetoothDevice, @NonNull BluetoothGattCharacteristic param1BluetoothGattCharacteristic, byte[] param1ArrayOfbyte, ADBlePeripheral.ADWriteCharacteristicCallback param1ADWriteCharacteristicCallback) {
            super(param1BluetoothDevice);
            this.mCharacteristic = param1BluetoothGattCharacteristic;
            this.mBytes = param1ArrayOfbyte;
            this.mCallback = param1ADWriteCharacteristicCallback;
        }

        public void run() {
            ADLog.v(ADBleService.TAG, "%s writeValueForCharacteristic", new Object[]{ADBleOperator.class.getName()});
            BluetoothGatt bluetoothGatt = ADBleService.this.getBluetoothGatt(getBluetoothDevice());
            if (bluetoothGatt == null && this.mBytes != null)
                return;
            BluetoothGattCharacteristic bluetoothGattCharacteristic = this.mCharacteristic;
            if (bluetoothGattCharacteristic == null)
                return;
            if ((bluetoothGattCharacteristic.getProperties() & 0x1) > 0) {
                this.mCharacteristic.setWriteType(1);
            } else {
                this.mCharacteristic.setWriteType(2);
            }
            this.mCharacteristic.setValue(this.mBytes);
            bluetoothGatt.writeCharacteristic(this.mCharacteristic);
            ADBlePeripheral.ADWriteCharacteristicCallback aDWriteCharacteristicCallback = this.mCallback;
            if (aDWriteCharacteristicCallback != null)
                aDWriteCharacteristicCallback.writeCharacteristicFinish();
        }
    }

    public class LocalBinder extends Binder {
        ADBleService getService() {
            return ADBleService.this;
        }
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBleService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */