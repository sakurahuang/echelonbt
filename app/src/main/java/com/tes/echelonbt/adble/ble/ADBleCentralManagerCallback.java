package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanRecord;

@TargetApi(18)
public interface ADBleCentralManagerCallback {
    void bleCentralManagerDidConnectPeripheral(ADBlePeripheral paramADBlePeripheral);

    void bleCentralManagerDidDisconnectPeripheral(ADBlePeripheral paramADBlePeripheral);

    void bleCentralManagerDidDiscover(ADBlePeripheral paramADBlePeripheral, int paramInt, ScanRecord paramScanRecord);

    void bleCentralManagerDidInitialized();
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBleCentralManagerCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */