package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Handler;

import com.tes.echelonbt.appdevice.adble.utility.ADLog;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.tes.echelonbt.appdevice.adble.utility.ADConverter;
import com.tes.echelonbt.santoble.SharedHandlerThread;
import com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheralConnectionStautsCallback;
import com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheralCallback;
import com.tes.echelonbt.appdevice.adble.ble.ADBleService;

@TargetApi(18)
public class ADBlePeripheral {
    private static String TAG = "ADBle";

    private int mBleConnectionStatus = 0;

    private ADBleService mBleService = null;

    private BluetoothDevice mBluetoothDevice = null;

    private ADBlePeripheralCallback mCallback = null;

    private ADBlePeripheralConnectionStautsCallback mConnectionStautsCallback;

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        public void onCharacteristicChanged(BluetoothGatt param1BluetoothGatt, BluetoothGattCharacteristic param1BluetoothGattCharacteristic) {
            System.out.println("========onCharacteristicChanged=====");
            byte[] arrayOfByte = param1BluetoothGattCharacteristic.getValue();
            mCallback.peripheralDidUpdateValueForCharacteristic(ADBlePeripheral.this, param1BluetoothGattCharacteristic, arrayOfByte);
        }

        public void onCharacteristicRead(BluetoothGatt param1BluetoothGatt, BluetoothGattCharacteristic param1BluetoothGattCharacteristic, int param1Int) {
            System.out.println("=====onCharacteristicRead===");
            if (mCallback == null)
                return;
            mCallback.peripheralDidReadValueForCharacteristic(ADBlePeripheral.this, param1BluetoothGattCharacteristic, param1Int);
            mBleService.onOperatorFinish();

        }

        public void onCharacteristicWrite(BluetoothGatt param1BluetoothGatt, BluetoothGattCharacteristic param1BluetoothGattCharacteristic, int param1Int) {
            System.out.println("======onCharacteristicWrite=======");
            if (mCallback == null)
                return;
            mCallback.peripheralDidWriteValueForCharacteristic(ADBlePeripheral.this, param1BluetoothGattCharacteristic, param1Int);
            mBleService.onOperatorFinish();
        }

        public void onConnectionStateChange(BluetoothGatt param1BluetoothGatt, int param1Int1, int param1Int2) {
            System.out.println("==========onConnectionStateChange==========" + param1Int2);
            if (param1Int2 != 3) {
                if (param1Int2 == 0)
                    ADBlePeripheral.this.mBleService.onDisconnect(ADBlePeripheral.this.mBluetoothDevice);
                if (ADBlePeripheral.this.mBleConnectionStatus == 1 && (param1Int1 == 0 || param1Int1 == 133))
                    ADBlePeripheral.this.mBleService.onOperatorFinish();
                ADBlePeripheral.this.setBleConnectionStatus(param1Int2);
            }
        }

        public void onDescriptorWrite(BluetoothGatt param1BluetoothGatt, BluetoothGattDescriptor param1BluetoothGattDescriptor, int param1Int) {
            System.out.println("========onDescriptorWrite====");
            if (mCallback == null)
                return;
            mCallback.peripheralDidWriteValueForDescriptor(ADBlePeripheral.this, param1BluetoothGattDescriptor, param1Int);
            mBleService.onOperatorFinish();
        }

        public void onReadRemoteRssi(BluetoothGatt param1BluetoothGatt, int param1Int1, int param1Int2) {
            System.out.println("========onReadRemoteRssi==========");
            if (mCallback == null)
                return;
            mCallback.peripheralDidReadRSSI(ADBlePeripheral.this, param1Int1, param1Int2);
            mBleService.onOperatorFinish();
        }

        public void onServicesDiscovered(BluetoothGatt param1BluetoothGatt, int param1Int) {
            System.out.println("======onServicesDiscovered=======");
            if (mCallback == null)
                return;
            mCallback.peripheralDidDiscoverServices(ADBlePeripheral.this,  param1BluetoothGatt.getServices().toArray(new BluetoothGattService[0]), param1Int);
            mBleService.onOperatorFinish();
        }

    };

    private Handler mHandler = SharedHandlerThread.createHandler();

    private List<BluetoothGattService> mServices = new ArrayList<BluetoothGattService>();

    ADBlePeripheral(@NonNull BluetoothDevice paramBluetoothDevice, @NonNull ADBleService paramADBleService) {
        if (paramBluetoothDevice != null) {
            if (paramADBleService != null) {
                this.mBluetoothDevice = paramBluetoothDevice;
                this.mBleService = paramADBleService;
                return;
            }
            throw new IllegalArgumentException("bleService = null");
        }
        throw new IllegalArgumentException("bluetoothDevice = null");
    }

    void cancelConnect() {
        if (this.mBleConnectionStatus == 0)
            return;
        this.mBleService.cancelConnect(this.mBluetoothDevice);
    }

    void connect() {
        if (this.mBleConnectionStatus == 0) {
            setBleConnectionStatus(1);
            this.mBleService.connect(this.mBluetoothDevice, this.mGattCallback);
        }
    }

    public void discoverServices() {
        mServices.clear();
        if (mBleService != null)
            mBleService.discoverServices(mBluetoothDevice);
    }

    public String getAddress() {
        return this.mBluetoothDevice.getAddress();
    }

    public int getBleConnectionStatus() {
        return this.mBleConnectionStatus;
    }

    public String getName() {
        return this.mBluetoothDevice.getName();
    }

    public BluetoothGattService[] getService() {
        List<BluetoothGattService> list = this.mServices;
        return list.<BluetoothGattService>toArray(new BluetoothGattService[list.size()]);
    }

    public void readRssi() {
        if (this.mBleConnectionStatus != 2)
            return;
        ADBleService aDBleService = this.mBleService;
        if (aDBleService != null) {
            BluetoothDevice bluetoothDevice = this.mBluetoothDevice;
            if (bluetoothDevice != null)
                aDBleService.readRssi(bluetoothDevice);
        }
    }

    public void readValueForCharacteristic(@NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic) {
        if (this.mBleConnectionStatus != 2)
            return;
        ADBleService aDBleService = this.mBleService;
        if (aDBleService != null) {
            BluetoothDevice bluetoothDevice = this.mBluetoothDevice;
            if (bluetoothDevice != null)
                aDBleService.readCharacteristic(bluetoothDevice, paramBluetoothGattCharacteristic);
        }
    }

    void setBleConnectionStatus(int paramInt) {
        mBleConnectionStatus = paramInt;
        if (mConnectionStautsCallback == null)
            return;
        mConnectionStautsCallback.didConnectionStatusChanged(this, paramInt);
    }

    public void setCallback(ADBlePeripheralCallback paramADBlePeripheralCallback) {
        mCallback = paramADBlePeripheralCallback;
    }

    void setConnectionStautsCallback(ADBlePeripheralConnectionStautsCallback paramADBlePeripheralConnectionStautsCallback) {
        mConnectionStautsCallback = paramADBlePeripheralConnectionStautsCallback;
    }

    public void setNotifyValueForCharacteristic(@NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic, boolean paramBoolean) {
        if (this.mBleConnectionStatus != 2)
            return;
        ADBleService aDBleService = this.mBleService;
        if (aDBleService != null) {
            BluetoothDevice bluetoothDevice = this.mBluetoothDevice;
            if (bluetoothDevice != null)
                aDBleService.setCharacteristicNotification(bluetoothDevice, paramBluetoothGattCharacteristic, paramBoolean);
        }
    }

    public void writeValueForCharacteristic(@NonNull BluetoothGattCharacteristic paramBluetoothGattCharacteristic, @NonNull byte[] paramArrayOfbyte, ADWriteCharacteristicCallback paramADWriteCharacteristicCallback) {
        if (this.mBleConnectionStatus != 2)
            return;
        if (this.mBleService != null && this.mBluetoothDevice != null) {
            String str2 = TAG;
            String str1 = ADConverter.byteArrayToHexString(paramArrayOfbyte);
            int i = 0;
            ADLog.i(str2, "SND %s", new Object[]{str1});
            while (true) {
                int k;
                int j = i + 20;
                if (j <= paramArrayOfbyte.length) {
                    k = j;
                } else {
                    k = paramArrayOfbyte.length + i - i;
                }
                byte[] arrayOfByte = Arrays.copyOfRange(paramArrayOfbyte, i, k);
                if (this.mBleConnectionStatus != 2)
                    return;
                if (k >= paramArrayOfbyte.length) {
                    this.mBleService.writeCharacteristic(this.mBluetoothDevice, paramBluetoothGattCharacteristic, arrayOfByte, paramADWriteCharacteristicCallback);
                    break;
                }
                this.mBleService.writeCharacteristic(this.mBluetoothDevice, paramBluetoothGattCharacteristic, arrayOfByte, null);
                i = j;
            }
        }
    }

    public static interface ADWriteCharacteristicCallback {
        void writeCharacteristicFinish();
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBlePeripheral.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */