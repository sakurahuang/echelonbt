package com.tes.echelonbt.appdevice.adble.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.tes.echelonbt.appdevice.adble.ble.ADBlePeripheral;

@TargetApi(18)
public interface ADBlePeripheralCallback {
  void peripheralDidDiscoverServices(ADBlePeripheral paramADBlePeripheral, BluetoothGattService[] paramArrayOfBluetoothGattService, int paramInt);
  
  void peripheralDidReadRSSI(ADBlePeripheral paramADBlePeripheral, int paramInt1, int paramInt2);
  
  void peripheralDidReadValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int paramInt);
  
  void peripheralDidUpdateValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, byte[] paramArrayOfbyte);
  
  void peripheralDidWriteValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int paramInt);
  
  void peripheralDidWriteValueForDescriptor(ADBlePeripheral paramADBlePeripheral, BluetoothGattDescriptor paramBluetoothGattDescriptor, int paramInt);
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/appdevice/adble/ble/ADBlePeripheralCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */